<?php

class sfWidgetFormAmKeyboardSwitcherInputText extends sfWidgetFormInputText
{
    const amKeyboardSwitcherInputClassName = "amKeyboardSwitcherInput";
    
  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes);
    $this->addOption('amKeyboards', array('english', 'bijoy', 'unijoy', 'phonetic_int'));
  }
  
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $textElementId = $this->generateId($name, $value);
    $amContainerId = 'amKeyboardSwitcherContainer' . uniqid('am_');
    
    return "<span id='$amContainerId' class='amKeyboardSwitcherContainer'>" . $this->renderTag('input', array_merge(array('type' => $this->getOption('type'), 'name' => $name, 'value' => $value, 'class' => self::amKeyboardSwitcherInputClassName), $attributes)) .
              $this->getAmKeyboardSwitcher($textElementId, $amContainerId) . "</span>" . $this->getJs($textElementId, $amContainerId);
  }
  
  protected function getAmKeyboardSwitcher($textElementId, $amContainerId)
  {
    $amKeyboards_showable = $amKeyboards = sfConfig::get('app_amKeyboards');
    
    if(is_array($preferredAmKeyboards = $this->getOption('amKeyboards')) && count($preferredAmKeyboards))
    {
      $amKeyboards_showable = array();

      foreach($preferredAmKeyboards as $value)
        if(array_key_exists($value, $amKeyboards))
          $amKeyboards_showable[$value] = $amKeyboards[$value];
    }
    
    $keyboardsHTML = '';
    
    foreach($amKeyboards_showable as $name => $anAmKeyboard)
    {
      $jsMethodName = 'enable' . ucwords($name);
      if(isset($anAmKeyboard['jsMethodName']))
        $jsMethodName = $anAmKeyboard['jsMethodName'];
        
      $jsMethodName .= "(\"$textElementId\")";
      
      $linkContent = $anAmKeyboard['title'];
      if(isset($anAmKeyboard['image']))
        $linkContent = image_tag("/amKeyboardSwitcherPlugin/images/$name/{$anAmKeyboard['image']}", array('alt' => $anAmKeyboard['title'], 'title' => $anAmKeyboard['title']));
      
      $keyboardsHTML .= <<<KEYBOARD_HTML
        <a href='#' onclick='$jsMethodName; highlightSelectedKeyboardLinks_$amContainerId(this); return false;'>
          $linkContent
        </a>
KEYBOARD_HTML;
    }
        
    return <<<amKeyboardSwitcher
      <span class="amKeyboardSwitcherLinkContainer">
        $keyboardsHTML
      </span>
amKeyboardSwitcher;
  }
  
  public function getJs($textElementId, $amContainerId) {
    return <<<JS
      <script type="text/javascript">
        //registering evenets
        jQuery("#$textElementId").focus(function(e) {
            var toolBox = jQuery(this).parent().children('.amKeyboardSwitcherLinkContainer')[0];
            jQuery(this).parent().children(".amKeyboardSwitcherLinkContainer").css({
                'display': 'inline',
                'left': jQuery(this).width() + 20 + 'px',
                'bottom': jQuery(this).height() - jQuery(toolBox).height() + 4 + 'px'
            });
        });
        
        function highlightSelectedKeyboardLinks_$amContainerId(linkObj)
        {
          var amSpans = document.getElementById('$amContainerId').getElementsByClassName('amKeyboardSwitcherLinkContainer');
          for(i=0; i<amSpans.length; i++){
            var amLinks = amSpans[i].getElementsByTagName('a');
            for(j=0; j<amLinks.length; j++)
            {
              unhighlighted_$amContainerId(amLinks[j]);
              
              var highlightable = false;
              if(amLinks[j] === linkObj)
                highlightable = true;
                
              var amImages = amLinks[j].getElementsByTagName('img');
              
              for(k=0; k<amImages.length; k++){
                if(highlightable){
                  highlighted_$amContainerId(amImages[k]);
                  highlightable = false;
                }
                else
                  unhighlighted_$amContainerId(amImages[k]);
              }
              
              if(highlightable)
                highlighted_$amContainerId(linkObj);
            }
          }
          document.getElementById('$textElementId').focus();
        }
        
        function highlighted_$amContainerId(obj) {
          obj.style.border = '1px solid white';
        }
        
        function unhighlighted_$amContainerId(obj) {
          obj.style.border = 'none';
        }
      </script>
JS;
  }
}
