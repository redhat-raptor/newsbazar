<?php
    $am_keyboard_switcher_js_path = 'amKeyboardSwitcherPlugin/js';
    $am_keyboard_switcher_css_path = 'amKeyboardSwitcherPlugin/css';
    sfContext::getInstance()->getResponse()->addJavascript("/$am_keyboard_switcher_js_path/am_keyboard_switcher.js");
    sfContext::getInstance()->getResponse()->addStyleSheet("/$am_keyboard_switcher_css_path/am_keyboard_switcher.css");
    
    $amKeyboards = sfConfig::get('app_amKeyboards');
    foreach($amKeyboards as $anAmKeyboardName => $anAmKeyboard) {
        if(isset($anAmKeyboard['hasJsFile']) && $anAmKeyboard['hasJsFile'] == false )
            continue;
        
        $vendorJsName = isset($anAmKeyboard['jsFileName']) ? $anAmKeyboard['jsFileName'] : "$anAmKeyboardName.js";
        sfContext::getInstance()->getResponse()->addJavascript("/$am_keyboard_switcher_js_path/$anAmKeyboardName/$vendorJsName");
    }
    