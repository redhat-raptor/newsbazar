
/* added shift intelligency by Hasin Hayder, 14th March 2008 */
/* added  accent key ( ` ) as joiner.   (By Sabuj Kundu, 17th March 2008) */

// Set of Characters
var activeta; //active text area
var avro=new Array();
var shift=false; //for intelligent shift
// avro bangla equivalents
//Bengali: {U+0980..U+09FF} 

//digits
avro['0']='\u09e6';//'০'; 
avro['1']='\u09e7';//'১';
avro['2']='\u09e8';//'২';
avro['3']='\u09e9';//'৩';
avro['4']='\u09ea';//'৪';
avro['5']='\u09eb';//'৫';
avro['6']='\u09ec';//'৬';
avro['7']='\u09ed';//'৭';
avro['8']='\u09ee';//'৮';
avro['9']='\u09ef';//'৯';

//----
avro['a']='\u09BE'; // a kar
avro['e']='\u09C7'; // e kar
avro['i']='\u09BF'; // hrossho i kar
avro['I']='\u09C0'; // dirgho i kar
avro['rri'] ='\u09C3';// wri kar
avro['u'] = '\u09C1'; // hrossho u kar
avro['U'] = '\u09C2'; // dirgho u kar
avro['OU']='\u099C'; // ou kar
avro["."] ="\u0964"; // dari
avro[',,'] = '\u09CD' + '\u200c'; // hosonto

//----
avro['k'] = '\u0995'; // ko
avro['kh'] = '\u0996'; // kho
avro['g']='\u0997';	// go
avro['gh']='\u0998'; // gho
avro['Ng']='\u0999';	// Uma

//----
avro['c']='\u099A'; //  cho
avro['ch']='\u099B'; // ccho
//avro['C']='\u099B'; // ccho
avro['j']='\u099C';	// borgio jo
//avro['J']='\u099D'; // jho
avro['jh']='\u099D'; // jho
avro['NG']='\u099E';	// yo

//----
avro['T']='\u099f'; // to
avro['Th']='\u09A0'; // tho
avro['D']='\u09A1'; // ddo
avro['Dh']='\u09A2'; // ddho
avro['N']='\u09A3'; // murdhonyo no

//----
avro['t']='\u09A4'; // tto
avro['th']='\u09A5'; // ttho
avro['d']='\u09A6'; // do
avro['dh']='\u09A7'; // dho
avro['n']='\u09A8'; // dontyo no

//----
avro['p']='\u09AA'; // po
avro['f']='\u09AB'; // fo
avro['ph']='\u09AB'; // fo
avro['b']='\u09AC'; // bo
avro['bh']='\u09AD'; // bho
avro['v']='\u09AD'; // bho
avro['m']='\u09AE';	// mo

//----
avro['z']='\u09AF';// ontoshyo zo
avro['J']='\u09AF';// ontoshyo zo
avro['r']='\u09B0'; // ro
avro['l']='\u09B2';	// lo
avro['sh']='\u09B6';	// talobyo sho
avro['S']='\u09B6';	// talobyo sho
avro['Sh']='\u09B7'; // mordhonyo sho

//----
avro['s']='\u09B8'; // dontyo so
avro['h']='\u09B9';	// ho
//avro['rh']='o';	 // doye bindu ro
avro['R']='\u09DC';	 // doye bindu ro
avro['Rh']='\u09DD';	 // dhoye bindu ro
avro['y']='\u09DF';	// ontostho yo
avro['Y']='\u09DF';	// ontostho yo
//avro['G']='\u0998';	// gho

//---- 
avro['t``']='\u09CE'; // tto NW
avro['ng']='\u0982';	// uniswor
avro[':']='\u0983'; // bisworgo
avro['^'] = '\u0981'; // chondrobindu

//---- Sorborno
avro['o']='\u0985'; // shore o 
avro['a']='\u0986'; // shore a
avro['i']='\u0987'; // hrossho i
avro['I']='\u0988'; // dirgho i
avro['ee']='\u0988'; // dirgho i
avro['u'] = '\u0989'; // hrossho u
avro['oo'] = '\u0989'; // hrossho u
avro['U'] = '\u098A'; // dirgho u
avro['rri']='\u098B'; // wri
avro['e'] = '\u098F'; // E
//avro['Oi']='\u0990'; // Oi
avro['OI']='\u0990'; // Oi
avro['O']= '\u0993';//'\u09CB'; // o
avro['OU']='\u0994'; // OU
//avro['Ou']='\u0994'; // OU


avro[".."] = "."; // fullstop
//avro['Y'] ='\u09CD'+'\u09AF'; // jo fola
//avro['w'] ='\u09CD'+ '\u09AC'; // wri kar

//avro['wr'] ='\u09C3'; // wri kar
/*avro['x'] ="\u0995"  + '\u09CD'+ '\u09B8';
avro['rY'] = avro['r']+ '\u200D'+ '\u09CD'+'\u09AF';
avro['L'] = avro['l'];
avro['Z'] = avro['z'];
avro['P'] = avro['p'];
avro['V'] = avro['v'];
avro['B'] = avro['b'];
avro['M'] = avro['m'];
avro['V'] = avro['v'];
avro['X'] = avro['x'];
avro['V'] = avro['v'];
avro['F'] = avro['f'];
avro['vowels']='aIiUuoiiouueEiEu'; //dont change this pattern*/
avro['vowels']=''; //dont change this pattern
//End Set


var carry = '';  //This variable stores each keystrokes
var old_len =0; //This stores length parsed bangla charcter
var ctrlPressed=false;
var len_to_process_oi_kar=0;
var first_letter = false;
var carry2="";
isIE=document.all? 1:0;
var switched=false;

function checkKeyDown(ev)
{
	//just track the control key
	var e = (window.event) ? event.keyCode : ev.which;
	if (e=='17')
	{
		ctrlPressed = true;
	}
	else if(e==16)
	shift=true;
}

function checkKeyUp(ev)
{
	//just track the control key
	var e = (window.event) ? event.keyCode : ev.which;
	if (e=='17')
	{
		ctrlPressed = false;
		//alert(ctrlPressed);
	}

}



function parseAvro(evnt)
{
	// main avro parser
	var t = document.getElementById(activeta); // the active text area
	var e = (window.event) ? event.keyCode : evnt.which; // get the keycode
	

	var browser=navigator.appName;

	if (browser=="Netscape")
		var charCode = (evnt.which) ? evnt.which : evnt.keyCode;
	else if(browser=="Microsoft Internet Explorer")
		var charCode = e;

	if (charCode==123)
	{
		//switch the keyboard mode
			switched = !switched;
			//alert("H"+switched);
			return true;
	}

	if (switched) return true;
	
	if(ctrlPressed)
	{
		// user is pressing control, so leave the parsing
		e=0; 
	}

	if (shift)
	{
		var char_e = String.fromCharCode(e).toUpperCase(); // get the character equivalent to this keycode
		shift=false;
	}
	else
	var char_e = String.fromCharCode(e); // get the character equivalent to this keycode
	
	if(e==8 || e==32)
	{
		// if space is pressed we have to clear the carry. otherwise there will be some malformed conjunctions
		carry = " ";	
		old_len = 1;
		return;
	}

	lastcarry = carry;
	carry += "" + char_e;	 //append the current character pressed to the carry
	
	//the intellisense
	if ((avro['vowels'].indexOf(lastcarry)!=-1 && avro['vowels'].indexOf(char_e)!=-1) || (lastcarry==" " && avro['vowels'].indexOf(char_e)!=-1) )
	{
		//let's check for dhirgho i kar and dhirgho u kar :P	
		if(carry=='ii' || carry=='uu' )
		{
			//char_e = char_e.toUpperCase();
			carry = lastcarry+char_e;
			//alert('carry1='+carry);
		}
		else
		{
			char_e = char_e.toUpperCase();
			carry = lastcarry+char_e;
			//alert('carry2='+carry);
		}
	}
	//intellisense ended
	
	bangla = parseAvroCarry(carry); // get the combined equivalent
	tempBangla = parseAvroCarry(char_e); // get the single equivalent

	if (tempBangla == ".." || bangla == "..") //that means it has next sibling
	{
		return false;
	}
	//alert(carry);
	if (char_e=="+" || char_e=="="||char_e=="`")
	{
		if(carry=="++" || carry=="=="||carry=="``")
		{
			// check if it is a plus/equal/accent  key/sign
			insertConjunction(char_e,old_len);
			old_len=1;
			return false;
		}	
		//otherwise this is a simple joiner
		insertAtCursor("\u09CD");old_len = 1;
		carry2=carry;
		carry=char_e;
		return false;
	}
	
	
	else if(old_len==0) //first character
	{
		// this is first time someone press a character
		insertConjunction(bangla,1);
		old_len=1;
		return false;
		
	}
/*	else if((char_e=='z' || char_e=='Z')&& carry2=="r+")//force Za-phola after Ra
	{
		//alert('yes');
		insertConjunction('\u200D'+'\u09CD'+avro['z'],1);
		old_len=1;	
		return false;
	} */
	
	else if(carry=="Ao")
	{
		// its a shore o
		insertConjunction(parseAvroCarry("ao"),old_len);
		old_len=1;
		return false;
	}
	else if (carry == "ii")
	{
		// process dirgho i kar
		//alert('dirgho i kar');
		insertConjunction(avro['ii'],1);
		old_len = 1;
		return false;
	}
	
	else if (carry == "oI" )
	{
		insertAtCursor('\u09C8',1);
		return false;
	}		

	else if (char_e == "o")
	{
		old_len = 1;
		insertAtCursor('\u09CB');
		carry = "o";
		return false;
	}
	
	
	else if (carry == "oU")
	{
		// ou kar
		insertConjunction("\u09CC",old_len);
		old_len = 1;
		return false;
	}	
	
	else if((bangla == "" && tempBangla !="")) //that means it has no joint equivalent
	{
		
		// there is no joint equivalent - so show the single equivalent. 
		bangla = tempBangla;
		if (bangla=="")
		{
			// there is no available equivalent - leave as is
			carry ="";
			return;
		}
		
		else
		{
			// found one equivalent
			carry = char_e;
			insertAtCursor(bangla);
			old_len = bangla.length;
			return false;
		}
	}
	else if(bangla!="")//joint equivalent found 
	{
		// we have found some joint equivalent process it
		
		insertConjunction(bangla, old_len);
		old_len = bangla.length;
		return false;
	}
}

    function parseAvroCarry(code)
    {
	//this function just returns a bangla equivalent for a given keystroke
	//or a conjunction
	//just read the array - if found then return the bangla eq.
	//otherwise return a null value
        if (!avro[code])  //Oh my god :-( no bangla equivalent for this keystroke

        {
			return ''; //return a null value
        }
        else
        {
            return ( avro[code]);  //voila - we've found bangla equivalent
        }

    }


function insertAtCursor(myValue) {
	/**
	 * this function inserts a character at the current cursor position in a text area
	 * many thanks to alex king and phpMyAdmin for this cool function
	 * 
	 * This function is originally found in phpMyAdmin package and modified by Hasin Hayder to meet the requirement
	 */
	var myField = document.getElementById(activeta);
	if (document.selection) {		
		myField.focus();
		sel = document.selection.createRange();
		sel.text = myValue;
		sel.collapse(true);
		sel.select();
	}
	//MOZILLA/NETSCAPE support
	else if (myField.selectionStart || myField.selectionStart == 0) {
		
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		var scrollTop = myField.scrollTop;
		startPos = (startPos == -1 ? myField.value.length : startPos );
		myField.value = myField.value.substring(0, startPos)
		+ myValue
		+ myField.value.substring(endPos, myField.value.length);
		myField.focus();
		myField.selectionStart = startPos + myValue.length;
		myField.selectionEnd = startPos + myValue.length;
		myField.scrollTop = scrollTop;
	} else {
		var scrollTop = myField.scrollTop;
		myField.value += myValue;
		myField.focus();
		myField.scrollTop = scrollTop;
	}
}

function insertConjunction(myValue, len) {
	/**
	 * this function inserts a conjunction and removes previous single character at the current cursor position in a text area
	 * 
	 * This function is derived from the original one found in phpMyAdmin package and modified by Hasin to meet our need
	 */
	//alert(len);
	var myField = document.getElementById(activeta);
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		if (myField.value.length >= len){  // here is that first conjunction bug in IE, if you use the > operator
			sel.moveStart('character', -1*(len));   
			//sel.moveEnd('character',-1*(len-1));
		}
		sel.text = myValue;
		sel.collapse(true);
		sel.select();
	}
	//MOZILLA/NETSCAPE support
	else if (myField.selectionStart || myField.selectionStart == 0) {
		myField.focus();
		var startPos = myField.selectionStart-len;
		var endPos = myField.selectionEnd;
		var scrollTop = myField.scrollTop;
		startPos = (startPos == -1 ? myField.value.length : startPos );
		myField.value = myField.value.substring(0, startPos)
		+ myValue
		+ myField.value.substring(endPos, myField.value.length);
		myField.focus();
		myField.selectionStart = startPos + myValue.length;
		myField.selectionEnd = startPos + myValue.length;
		myField.scrollTop = scrollTop;
	} else {
		var scrollTop = myField.scrollTop;
		myField.value += myValue;
		myField.focus();
		myField.scrollTop = scrollTop;
	}
	//document.getElementById("len").innerHTML = len;
}

function makeAvroEditor(textAreaId)
{
	activeTextAreaInstance = document.getElementById(textAreaId);
	activeTextAreaInstance.onkeypress = parseAvro; 
	activeTextAreaInstance.onkeydown = checkKeyDown; 
	activeTextAreaInstance.onkeyup = checkKeyUp;
	activeTextAreaInstance.onfocus = function(){activeta=textAreaId;};
}
function makeVirtualEditor(textAreaId)
{
	activeTextAreaInstance = document.getElementById(textAreaId);
	activeTextAreaInstance.onfocus = function(){activeta=textAreaId;};
}

function makeUniAvroEditor(textAreaId)
{
activeTextAreaInstance = document.getElementById(textAreaId);
	activeTextAreaInstance.onkeypress = parseAvro; 
	activeTextAreaInstance.onkeydown = checkKeyDown; 
	activeTextAreaInstance.onkeyup = checkKeyUp;
	activeTextAreaInstance.onfocus = function(){activeta=textAreaId;};
}