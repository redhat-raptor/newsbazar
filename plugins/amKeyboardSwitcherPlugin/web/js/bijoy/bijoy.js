/******************************************
    This file is part of Bangla Unicode Web Tools 2.

Copyright (C) 2007  S M Mahbub Murshed

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


	Authors: S M Mahbub Murshed, Arup Kamal
	Copyright: Authors
	Email: udvranto@yahoo.com, arup.kamal@gmail.com
	Version: 1.2.0
	Date: September 06, 2006, XX:XX GMT

*******************************************/
var IE=(document.all ? 1:0);

var LCUNI=0; // Last typed character converted into unicode
var LLCUNI=0; // Last before last typed character converted into unicode, used for avro
var LC=0; // Last english character typed
var LLC=0; // Last before last english character typed, used for Avro

var LC_KAR=0; // Last recognized pre-kar, needed for bijoy input
var LC_STRING=""; // Array of complex-borno or banjoborno 
		 // after last pre-kar input. Required for bijoy input


// Keyboard layout to follow
var BIJOY = 2;
var KeyBoardLayout = BIJOY;

var ctl_v_conversion = false; // should convert with ctrl+v?


/******************************************
	Maps an ASCII character to its equivalent
	unicode character according to Bijoy
	layout.

	Coded by : S M Mahbub Murshed
	Date: September 07, 2006
*******************************************/
var bijoy_keyboard_map = {
	"0":"০",
	"1":"১",
	"2":"২",
	"3":"৩",
	"4":"৪",
	"5":"৫",
	"6":"৬",
	"7":"৭",
	"8":"৮",
	"9":"৯",

	"a":"ৃ",
	"A":"র্",
	"d":"ি",
	"D":"ী",
	"s":"ু",
	"S":"ূ",
	"f":"া",
	"F":"অ",
	"g":"্",
	"G":"।",
	"h":"ব",
	"H":"ভ",
	"j":"ক",
	"J":"খ",
	"k":"ত",
	"K":"থ",
	"l":"দ",
	"L":"ধ",
	"z":"্র",
	"Z":"্য",
	"x":"ও",
	"X":"ৗ",
	"c":"ে",
	"C":"ৈ",
	"v":"র",
	"V":"ল",
	"b":"ন",
	"B":"ণ",
	"n":"স",
	"N":"ষ",
	"m":"ম",
	"M":"শ",

	"q":"ঙ",
	"Q":"ং",
	"w":"য",
	"W":"য়",
	"e":"ড",
	"E":"ঢ",
	"r":"প",
	"R":"ফ",
	"t":"ট",
	"T":"ঠ",
	"y":"চ",
	"Y":"ছ",
	"u":"জ",
	"U":"ঝ",
	"i":"হ",
	"I":"ঞ",
	"o":"গ",
	"O":"ঘ",
	"p":"ড়",
	"P":"ঢ়",
	"&":"ঁ",
	"$":"৳",
	"`":"\u200C",
	"~":"\u200D",

	"\\":"ৎ",
	"|":"ঃ"
}; // end bijoy_keyboard_map
/******************************************/


/******************************************
	Maps an ASCII character to its equivalent
	unicode character according to selected
	layout.

	\param C The ASCII character to find its Map

	Coded by : S M Mahbub Murshed
	Date: August 30, 2006
******************************************/
function MapUnicodeCharacter(C)
{
	return bijoy_keyboard_map[C];
} // end function MapUnicodeCharacter


/******************************************
	Resets Kar modifier tracking.

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function ResetKarModifier()
{
	LC_KAR = 0;
	LC_STRING = "";
} // end function ResetKarModifier





/******************************************
	Modifies a kar

	\param CUni Current charater

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function KarModification(field, CUni)
{
	if (LC_KAR == LCUNI || IsBanglaHalant(LCUNI) || CUni=="্র" || CUni=="্য")
	{
		var len = LC_STRING.length;
		LC_STRING = LC_STRING + CUni;
		RemoveNInsert(field, LC_STRING + LC_KAR,len+LC_KAR.length);
	}
	else if(CUni=="র্")
	{
		var len = LC_STRING.length;
		LC_STRING = CUni + LC_STRING;
		RemoveNInsert(field, LC_STRING + LC_KAR,len+LC_KAR.length);
	}
	else if(IsBanglaHalant(CUni))
	{
		LC_STRING = LC_STRING + CUni;
		Insert(field, CUni);
	}
	else
	{
		Insert(field, CUni);
		ResetKarModifier();
	}
} // end function KarModification

/******************************************
	Modifies a ref insertion

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function RefModification(field)
{
	var len = 1;
	var kar = "";
	var str = "";
	var halant_found = true;
	var CH = "";
		field.focus();
	while(true)
	{
		if (document.selection) 
		{
			sel = document.selection.createRange();
			if (field.value.length >= len)
			{
				sel.moveStart('character', -1 * len);   
			}
			else
			{
				CH = "",
				len--;
				sel.moveStart('character', -1 * len);
				break;
			}
			CH = sel.text.charAt(0);
		}
		else if (field.selectionStart || field.selectionStart == 0) 
		{
			var startPos = field.selectionStart-len;
			var endPos = field.selectionEnd;
			var scrollTop = field.scrollTop;

			if(startPos <0)
			{
				CH = "",
				len--;
				startPos = field.selectionStart-len;
				break;
			} 
			CH = field.value.substring(startPos, startPos+1)

		}

		if(len!=1 && IsBanglaKar(CH))
			break;

		if(len==1 && IsBanglaKar(CH))
			kar=CH;
		else if(IsBanglaSoroborno(CH) || IsBanglaDigit(CH) || IsSpace(CH))
			break;
		else if(IsBanglaBanjonborno(CH))
		{
			if(halant_found==true)
			{
				str = CH + str;
				halant_found = false;
			}
			else
				break;
		}
		else if(IsBanglaHalant(CH))
		{
			str = CH + str;
			halant_found = true;
		}
		len++;
	}

	var line = CH + "র্" + str + kar;
	if (document.selection) 
	{
		sel.text = line;
		sel.collapse(true);
		sel.select();
	}
	else if (field.selectionStart || field.selectionStart == 0) 
	{
		field.value = field.value.substring(0, startPos)
				+ line
				+ field.value.substring(endPos, field.value.length);
		field.focus();
		field.selectionStart = startPos + line.length;
		field.selectionEnd = startPos + line.length;
		field.scrollTop = scrollTop;
	}

} // end function RefModification





/******************************************
	Modifies a o-kar and ou-kar insertion

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function OAndOuKarModification(field, CH1, CH2)
{
	if (document.selection) 
	{
		field.focus();
		sel = document.selection.createRange();
		if (field.value.length >= 1)
			sel.moveStart('character', -1);   
		if(sel.text.charAt(0) == 'ে')
			sel.text = CH1;
		else
			sel.text = sel.text.charAt(0) + CH2;
		sel.collapse(true);
		sel.select();
	}
	else if (field.selectionStart || field.selectionStart == 0)
	{
		var startPos = field.selectionStart-1;
		var endPos = field.selectionEnd;
		var scrollTop = field.scrollTop;
		var CH;
		startPos = (startPos == -1 ? field.value.length : startPos );
		if(field.value.substring(startPos, startPos+1) == "ে")
			CH = CH1;
		else
		{
			startPos=startPos+1;
			CH = CH2;
		}
		field.value = field.value.substring(0, startPos)
			+ CH
			+ field.value.substring(endPos, field.value.length);
		field.focus();
		field.selectionStart = startPos + CH.length;
		field.selectionEnd = startPos + CH.length;
		field.scrollTop = scrollTop;
	}

} // end function OAndOuKarModification







/******************************************
	Processes an unicode charater input

	\param C the ascii character to process
	\param K the keyboard code for the ascii character to process
	\param CUni the unicode character to process

	Coded by : S M Mahbub Murshed
	Date: Septmeber 05, 2006
******************************************/
function ProcessCharacter(field, C, K, CUni)
{

	if(LCUNI ==0 && CUni =="্য")
	{
		RemoveNInsert(field, field.value.charAt(field.value.length-1) + "‌্য",1);
		ResetKarModifier();
		return;
	}


	// Skip if next kar is Post Kar
	if(IsBanglaPostKar(CUni))
		ResetKarModifier();

	// Skip numbers
	if(IsBanglaDigit(CUni))
		ResetKarModifier();

	if     (LCUNI=='অ' && CUni=='া')
	{
		RemoveNInsert(field, "আ",1); 
		ResetKarModifier();
	}
	else if (KeyBoardLayout==BIJOY && IsBanglaPreKar(LC_KAR))
		KarModification(field, CUni);
	else if (CUni=="র্") 
		RefModification(field);
	else if (K>29) { Insert(field, CUni); }
	else if (K==13 && IE) { Insert(field, CUni); }


	if( (IsBanglaHalant(LCUNI)==false && IsBanglaPreKar(CUni)))
		LC_KAR=CUni;

	if (!(IsBanglaNukta(LCUNI) && IsBanglaFola(CUni)))
	{
		LCUNI=CUni;
	}
} // end function ProcessCharacter





/******************************************
	Handles javascript keydown event

	\param ev the event

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function KeyBoardDown(ev)
{
	
 
	var field;
	if(IE)
	{
		var ev = window.event;
		field = ev.srcElement;
	}
	else
		field = ev.target;

	var K = (window.event) ? event.keyCode : ev.which;
	var C = String.fromCharCode(K);

	if(K==27 /*ESC*/)
		{ EnglishKeyboard = !EnglishKeyboard; ChangeKeyboarLayoutStatus(); }

	// Skip special characters
	if( (K >= 8 && K <= 13) 
	|| K==27 /*ESC*/ || K==32 /*SPACE*/ || K==46/*DEL*/ 
	|| (K>=37 && K<=40)/*ARROWS*/ )
	{
		LCUNI = 0;
		ResetKarModifier();
	}

	if(ev.altKey && ev.ctrlKey && (C=='B'||C=='b'))
		{ KeyBoardLayout = (KeyBoardLayout==BIJOY?ENGLISH:BIJOY); ChangeKeyboarLayoutStatus(); }
	
	if(K==27)
		return false;

	return true;
} // end function KeyBoardDown

/******************************************
	Handles javascript keypress event

	\param ev the event

	Coded by : Arup Kamal, S M Mahbub Murshed
	Date: August 28, 2006
******************************************/
function KeyBoardPress(ev)
{
	var field;
	if(IE)
	{
		var ev = window.event;
		field = ev.srcElement;
	}
	else
		field = ev.target;


	var K = (window.event) ? event.keyCode : ev.which;
	var C = String.fromCharCode(K);

var browser=navigator.appName;

	if (browser=="Netscape")
		var charCode = (ev.which) ? ev.which : ev.keyCode;
	else if(browser=="Microsoft Internet Explorer")
	{
		var charCode = e;
	}
	
	
	if (charCode==123)
	{
		//switch the keyboard mode
			switched = !switched;
			//alert("H"+switched);
			return true;
	}
	
	if (switched) return true;
	






	if(ev.altKey && ev.ctrlKey && (C=='E'||C=='e'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='B'||C=='b'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='P'||C=='p'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='A'||C=='a'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='U'||C=='u'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='V'||C=='v'))
		return false;
	else if(ev.altKey && ev.ctrlKey && (C=='O'||C=='o'))
		return false;

	else if(ev.ctrlKey || ev.altKey)
		return true;

	var CUni = "";
	CUni = MapUnicodeCharacter(C);
	if(CUni == null)
		return true;

	
	ProcessCharacter(field, C, K, CUni);

	if (IE) 
		event.keyCode=0;
	LC = C;

	if (K>29) return false;

	return true;
} // end function KeyBoardPress


/******************************************
******************************************
      General utility to insert
      text into the cursor position
******************************************
*******************************************/


/******************************************
	Function to insert a character into the text area.

	\param text The string or character to insert

	Coded by : Arup Kamal
	Date: August 26, 2006 (approx)
*******************************************/
function Insert(field, text)
{
	if (document.selection) 
	{
		field.focus();
		sel = document.selection.createRange();
		sel.text = text;
		sel.collapse(true);
		sel.select();
	}

	else if (field.selectionStart || field.selectionStart == '0')
	{

		
		var startPos = field.selectionStart;
		var endPos = field.selectionEnd;
		var scrollTop = field.scrollTop;
		startPos = (startPos == -1 ? field.value.length : startPos );
		field.value = field.value.substring(0, startPos)
			+ text
			+ field.value.substring(endPos, field.value.length);
		field.focus();
		field.selectionStart = startPos + text.length;
		field.selectionEnd = startPos + text.length;
		field.scrollTop = scrollTop;
	}
	else
	{
		var scrollTop = field.scrollTop;
		field.value += value;
		field.focus();
		field.scrollTop = scrollTop;
	}

} // end function Insert


/******************************************
	Function to delete a set of 
	character and insert a set of characters
	into the text area.

	\param value The string or character to insert
	\param len	Number of characters to delete from current cursor position

	Coded by : Arup Kamal
	Date: August 26, 2006 (approx)
*******************************************/
function RemoveNInsert(field, value, len) 
{
	if (document.selection) 
	{
		field.focus();
		sel = document.selection.createRange();
		if (field.value.length >= len)
			{ 			
			sel.moveStart('character', -1*(len));   
			}
		sel.text = value;
		sel.collapse(true);
		sel.select();
		}

	else if (field.selectionStart || field.selectionStart == 0) {
		field.focus();
		var startPos = field.selectionStart-len;
		var endPos = field.selectionEnd;
		var scrollTop = field.scrollTop;
		startPos = (startPos == -1 ? field.value.length : startPos );
		field.value = field.value.substring(0, startPos)
		+ value
		+ field.value.substring(endPos, field.value.length);
		field.focus();
		field.selectionStart = startPos + value.length;
		field.selectionEnd = startPos + value.length;
		field.scrollTop = scrollTop;
	} else {
		var scrollTop = field.scrollTop;
		field.value += value;
		field.focus();
		field.scrollTop = scrollTop;
	}
} // end function RemoveNInsert

/******************************************
	Function to detect caps lock
	Returns true if caps lock is on

	\param e The keypress event

	Coded by : http://www.howtocreate.co.uk/jslibs/htmlhigh/capsDetect.html
	Date: Sepetember 11, 2006
*******************************************/
function capsDetect( e )
{
	if( !e ) e = window.event;
	if( !e ) return false;
	//what (case sensitive in good browsers) key was pressed
	var theKey = e.which ? e.which : ( e.keyCode ? e.keyCode : ( e.charCode ? e.charCode : 0 ) );
	//was the shift key was pressed
	var theShift = e.shiftKey || ( e.modifiers && ( e.modifiers & 4 ) ); //bitWise AND
	//if upper case, check if shift is not pressed. if lower case, check if shift is pressed
	return ( ( theKey > 64 && theKey < 91 && !theShift ) || ( theKey > 96 && theKey < 123 && theShift ) );
}


/******************************************
*******************************************
      Bangla unicode utility
*******************************************
*******************************************/

/******************************************
	Determines whether the unicode
	character is an bangla digit
	character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaDigit(CUni)
{
	if(CUni=='০' || CUni=='১'
	|| CUni=='২' || CUni=='৩'
	|| CUni=='৪' || CUni=='৫'
	|| CUni=='৬' || CUni=='৭'
	|| CUni=='৮' || CUni=='৯')
		return true;

	return false;
} // end function IsBanglaDigit


/******************************************
	Determines whether the unicode
	character is a bangla pre kar
	character or not. Pre kar
	character are appended BEFORE
	a banjonborno or juktakhor character
	after rendering.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaPreKar(CUni)
{
	if(CUni=='ি' || CUni=='ৈ' 
	|| CUni=='ে' )
		return true;
	
	return false;
} // end function IsBanglaPreKar





/******************************************
	Determines whether the unicode
	character is a bangla post kar
	character or not. Post kar
	character are appended AFTER
	a banjonborno or juktakhor character
	after rendering.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaPostKar(CUni)
{
	if(CUni == 'া' || CUni=='ো'
	|| CUni=='ৌ' || CUni=='ৗ' || CUni=='ু'
	|| CUni=='ূ' || CUni=='ী'
	|| CUni=='ৃ')
		return true;
	return false;
} // end function IsBanglaPostKar




/******************************************
	Determines whether the unicode
	character is a bangla kar
	character or not. Its a super
	set or Pre and Post kars.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaKar(CUni)
{
	if(IsBanglaPreKar(CUni) || IsBanglaPostKar(CUni) )
		return true;
	return false;

} // end function IsBanglaKar






/******************************************
	Determines whether the unicode
	character is a bangla banjonborno
	character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaBanjonborno(CUni)
{
	if(CUni=='ক' || CUni=='খ' || CUni=='গ' || CUni=='ঘ' || CUni=='ঙ' 
        || CUni=='চ' || CUni=='ছ' || CUni=='জ' || CUni=='ঝ' || CUni=='ঞ' 
        || CUni=='ট' || CUni=='ঠ' || CUni=='ড' || CUni=='ঢ' || CUni=='ণ'
        || CUni=='ত' || CUni=='থ' || CUni=='দ' || CUni=='ধ' || CUni=='ন'
        || CUni=='প' || CUni=='ফ' || CUni=='ব' || CUni=='ভ' || CUni=='ম'
        || CUni=='শ' || CUni=='ষ' || CUni=='স' || CUni=='হ' 
	|| CUni=='য' || CUni=='র' || CUni=='ল' || CUni=='য়' 
	|| CUni=='ং' || CUni=='ঃ' || CUni=='ঁ'
	|| CUni=='ৎ')
		return true;

	return false;
} // end function IsBanglaBanjonborno




/******************************************
	Determines whether the unicode
	character is a bangla soroborno
	character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaSoroborno(CUni)
{
	if(CUni == 'অ' || CUni=='আ'
	|| CUni=='ই' || CUni=='ঈ'
	|| CUni=='উ' || CUni=='ঊ'
	|| CUni=='ঋ' || CUni=='ঌ'
	|| CUni=='এ' || CUni=='ঐ' 
	|| CUni=='ও' || CUni=='ঔ' )
		return true;

	return false;
} // end function IsBanglaSoroborno





/******************************************
	Determines whether the unicode
	character is a bangla nukta
	character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaNukta(CUni)
{
	if(CUni=='ং' || CUni=='ঃ' || CUni=='ঁ')
		return true;

	return false;

} // end function IsBanglaNukta






/******************************************
	Determines whether the unicode
	character is a bangla ja fola or ra fola
	character or not.

	\param CUni The Unicode string

	Coded by : S M Mahbub Murshed
	Date: August 30, 2006
*******************************************/
function IsBanglaFola(CUni)
{
	if(CUni=="্য" || CUni=="্র")
		return true;

	return false;
} // end function IsBanglaFola






/******************************************
	Determines whether the unicode
	character is a bangla halant
	character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsBanglaHalant(CUni)
{
	if(CUni=='্')
		return true;

	return false;
} // end function IsBanglaHalant


/******************************************
	Determines whether the unicode
	character is a bangla character or not.

	\param CUni The Unicode character

	Coded by : S M Mahbub Murshed
	Date: March 02, 2007
*******************************************/
function IsBangla(CUni)
{
	if(IsBanglaDigit(CUni) || IsBanglaKar(CUni) ||
	IsBanglaBanjonborno(CUni) || IsBanglaSoroborno(CUni) ||
	IsBanglaNukta(CUni) || IsBanglaFola(CUni) || IsBanglaHalant(CUni))
		return true;

	return false;
} // end function IsBangla

/******************************************
	Determines whether the character is 
	a ASCII character or not.

	\param CUni The character

	Coded by : S M Mahbub Murshed
	Date: March 02, 2007
*******************************************/
function IsASCII(CH)
{
	if(CH >= 0 && CH< 128)
		return true;

	return false;
} // end function IsBangla


/******************************************
	Determines whether the
	character is a space
	character or not.

	\param C The character

	Coded by : S M Mahbub Murshed
	Date: August 28, 2006
*******************************************/
function IsSpace(C)
{
	if( C==' ' ||  C=='\t' || C=='\n'
	||  C=='\r')
		return true;

	return false;
} // end function IsSpace



/******************************************
	Maps an unicode character to its
	equivalent soroborno

	\param CUni The Unicode kar character to map with

	Coded by : S M Mahbub Murshed
	Date: August 30, 2006
*******************************************/
function MapKarToSorborno(CUni)
{
	var CSorborno = '';
	if(CUni=='া')
		CSorborno = 'আ';
	else if(CUni=='ি')
		CSorborno = 'ই';
	else if(CUni=='ী')
		CSorborno = 'ঈ';
	else if(CUni=='ু')
		CSorborno = 'উ';
	else if(CUni=='ূ')
		CSorborno = 'ঊ';
	else if(CUni=='ৃ')
		CSorborno = 'ঋ';
	else if(CUni=='ে')
		CSorborno = 'এ';
	else if(CUni=='ৈ')
		CSorborno = 'ঐ';
	else if(CUni=='ো')
		CSorborno = 'ও';
	else if(CUni=="ো")
		CSorborno = 'ও';
	else if(CUni=='ৌ')
		CSorborno = 'ঔ';
	else if(CUni=="ৌ")
		CSorborno = 'ঔ';


	return CSorborno;
} // end function MapKarToSorborno





/******************************************
	Maps an unicode soroborono to its
	equivalent kar

	\param CUni The Unicode sorborno character to map with

	Coded by : S M Mahbub Murshed
	Date: August 30, 2006
*******************************************/
function MapSorbornoToKar(CUni)
{
	var CKar = '';
	if(CUni=='আ')
		CKar = 'া';
	else if(CUni=='ই')
		CKar = 'ি';
	else if(CUni=='ঈ')
		CKar = 'ী';
	else if(CUni=='উ')
		CKar = 'ু';
	else if(CUni=='ঊ')
		CKar = 'ূ';
	else if(CUni=='ঋ')
		CKar = 'ৃ';
	else if(CUni=='এ')
		CKar = 'ে';
	else if(CUni=='ঐ')
		CKar = 'ৈ';
	else if(CUni=='ও')
		CKar = 'ো';
	else if(CUni=='ঔ')
		CKar = 'ৌ';

	return CKar;
} // end function MapSorbornoToKar

// edited by Yasir Riad

function makebijoyEditor(textAreaId)
{    
	 KeyBoardLayout = BIJOY;
	activeTextAreaInstance = document.getElementById(textAreaId);
	
	activeTextAreaInstance.onkeypress = KeyBoardPress;
	activeTextAreaInstance.onkeydown = KeyBoardDown;
	//activeTextAreaInstance.onkeyup = checkKeyUp;
	activeTextAreaInstance.onfocus = function(){activeta=textAreaId;};
}


