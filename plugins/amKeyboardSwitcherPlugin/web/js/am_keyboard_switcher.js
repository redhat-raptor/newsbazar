function createCookie(name, value) {
    days = 256;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}


function changeKeyboard(KB_value)
{	
	if(KB_value!='') createCookie('keyboard_id',KB_value);
    keyboard_id = readCookie('keyboard_id');
    change_keyboard_field();
}

function setDefaultKayboard()
{
    var keyboard_id = readCookie('keyboard_id');
    
    if ( keyboard_id == null || keyboard_id == "" ) {
        keyboard_id = 2; //Assuming 2 is phonetix
        createCookie( 'keyboard_id', keyboard_id ); 
    }
    
    document.getElementById( 'keyboard_' + keyboard_id ).checked = true;
    change_keyboard_field();
}

var switched = false;
//setDefaultKayboard();  //on page load it tried to set user's default keyboard

function change_keyboard_field(current_obj_id)
{
    var keyboard_id = readCookie('keyboard_id');
	if( keyboard_id == '' ) keyboard_id = 2;	
    if( switched == true ) switched = false;
    
    /* optimize me */
    var oIds = new Array();
    
    var $inputs = $(':text');
    $inputs.each(function() {
        oIds[oIds.length] = $(this).attr('id');
    });
    
    var $textareas = $('textarea');
    $textareas.each(function() {
        oIds[oIds.length] = $(this).attr('id');
    });
    
    if( current_obj_id != undefined ) 
        oIds[oIds.length] = current_obj_id;
    
    if ( keyboard_id == '1' )        enableBijoy(oIds);
	else if ( keyboard_id == '2' )   enableUnijoy(oIds);
	else if ( keyboard_id == '3' )   enableAvroKeyboard(oIds);
    else if ( keyboard_id == '4' )   enablePhonetic(oIds);
    else if ( keyboard_id == '5' )   enableProbhat(oIds);
	else if ( keyboard_id == '6' )   enableEnglish(oIds);
}

function presetAmKeyboardSwitcher()
{
    if( switched == true ) switched = false;
}

function enableUnijoy( oId ) {
    presetAmKeyboardSwitcher();
    makeUnijoyEditor( oId );
}

function enableAvroKeyboard( oId ) {
    presetAmKeyboardSwitcher();
    makeUniAvroEditor( oId );
}

function enableBijoy( oId ) {
    presetAmKeyboardSwitcher();
    makebijoyEditor( oId );
}

function enablePhonetic_int( oId ) {
    presetAmKeyboardSwitcher();
    makePhoneticEditor( oId );
}

function enableProbhat( oId ) {
    presetAmKeyboardSwitcher();
    makeProbhatEditor( oId );
}

function enableEnglish() {
    switched = true;
}

jQuery(document).ready( function(){
    jQuery("*").focus(function(obj, e){
            if(jQuery(this).parents('.amKeyboardSwitcherLinkContainer').length == 0)
                jQuery('.amKeyboardSwitcherLinkContainer').hide();

            if(jQuery(this).hasClass("amKeyboardSwitcherInput"))
                jQuery(this).parents(".amKeyboardSwitcherContainer").children(".amKeyboardSwitcherLinkContainer").show();
	});
});