<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    date_default_timezone_set('UTC');
    $this->enablePlugins(array(
      'sfDoctrinePlugin',
      'sfDoctrineGuardPlugin',
      'amKeyboardSwitcherPlugin',
      'sfJqueryReloadedPlugin',
      'sfFormExtraPlugin',
      'sfAdminDashPlugin',
      'sfJqueryReloadedPlugin',
      'sfThumbnailPlugin',
      'sfImageTransformPlugin'
    ));
  }
}
