<?php use_helper('I18N') ?>

<?php slot('mainContainerHeader') ?>
    <h2><?php echo __('Signin', null, 'sf_guard') ?></h2>
<?php end_slot() ?>

<div id="mainContainerContent">
    <?php echo get_partial('sfGuardAuth/signin_form', array('form' => $form)) ?>
</div>