<?php use_helper('I18N') ?>

<div class="aLeftContent hasCremeYellowTheme hasMarginTop_10">
    <h3>Please login</h3>
    <div class="cont">
        <div class="block formContainer">
            <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
                <?php echo $form->renderHiddenFields() ?>
                <ul>
                    <li>
                        <?php echo $form['username']->renderError() ?>
                        <?php echo $form['username']->renderLabel() ?>
                        <?php echo $form['username']->render() ?>
                    </li>
                    <li>
                        <?php echo $form['password']->renderError() ?>
                        <?php echo $form['password']->renderLabel() ?>
                        <?php echo $form['password']->render() ?>
                    </li>
                    <li>
                        <?php echo $form['remember']->renderLabel('Remember me') ?>
                        <?php echo $form['remember']->render(array('style' => 'margin-top: 10px')) ?>
                    </li>
                </ul>

                <div class="form_actions block hasMarginTop_15">
                    <div class="nextActions">
                        <input class="amButton hasAmButtonTheme_GreenOK" type="submit" value="Login" />
                    </div>
                </div>
                <div class="clear-fix"></div>
            </form>
        </div>
    </div>
</div>