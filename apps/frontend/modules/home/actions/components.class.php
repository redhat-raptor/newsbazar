<?php

class homeComponents extends sfComponents
{
    public function executeMainNavigation()
    {
        $this->links = $this->getLinks( 'navpages' );
    }

    public function executeSubNavigation()
    {
        $this->links = $this->getLinks( 'sub_navpages' );
    }

    public function executeOptionalNavigation()
    {
        $this->links = $this->getLinks( 'optional_navpages' );
    }

    private function getLinks( $configName )
    {
        $this->current_route = '@' . sfContext::getInstance()->getRouting()->getCurrentRouteName();

        $navigations = sfConfig::get('app_navigations');
        $pages = $navigations[$configName];

        $links = array();
        foreach( $pages as $key => $aPage )
        {
            if( isset($aPage['is_visible']) && $aPage['is_visible'] == false ) continue;

            if ( !count($links) || ( isset( $aPage['is_separate'] ) ) )
                $links[] = array();

            $aPage['route'] = isset( $aPage['route'] ) ? $aPage['route'] : $key;
            $aPage['action'] = $key;

            if( $aPage['route'] == 'content' )
                $aPage['is_selected'] = $this->getRequestParameter('slug') ==  $key;
            else {
                if ( !$aPage['is_selected'] = $this->current_route == "@{$aPage['route']}" )
                    $aPage['is_selected'] = substr( $this->current_route, 1, strlen( $aPage['route'] ) ) == $aPage['route'];
            }

            $links[ count( $links ) -1 ][] = $aPage;
        }

        return $links;
    }
}
?>
