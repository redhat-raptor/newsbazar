<?php

/**
 * home actions.
 *
 * @package    ubn-sym
 * @subpackage home
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
  }
  
  public function executeImage(sfWebRequest $request)
  {
    $width = $request->getParameter('width', sfConfig::get('app_ubn_max_thumbnail_width'));
    $height = $request->getParameter('height', sfConfig::get('app_ubn_max_thumbnail_height'));
    $bgcolor = $request->getParameter('bgcolor', null);
    $bgcolor = $bgcolor ? '#'.$bgcolor : $bgcolor;
    $imageName = base64_decode($request->getParameter('name'));
    $uploadPath = sfConfig::get('sf_root_dir') . '/' .sfConfig::get('app_ubn_upload_path');

    /*
    $requestPathArray = $request->getPathInfoArray();
    $pathInfoArray = explode('/', $requestPathArray['PATH_INFO']);
    $imageName = $pathInfoArray[count($pathInfoArray)-1];
    */
    
    $filePath = $uploadPath . '/' . $imageName;
    $resizedFilePath = $uploadPath . '/' . "thumb_{$width}_{$height}_{$bgcolor}_{$imageName}";

    if(!file_exists($resizedFilePath))
    {
      $img = new sfImage($filePath);

      //if no bgcolor is set then it is for the full size pic... no padding should be added
      if(!$bgcolor)
          $img->thumbnail($width, $height, 'scale', null); //scale image with no bg
      else
          $img->thumbnail($width, $height, 'fit', $bgcolor); //fit image with bgcolor for thumbnail

      $img->setQuality(100);
      $img->saveAs($resizedFilePath);
    }
    
    $img = new sfImage($resizedFilePath);
    $response = $this->getResponse();
    $response->setContentType($img->getMIMEType());    
    $response->setContent($img);
    
    return sfView::NONE;
  }
}
