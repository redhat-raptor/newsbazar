<div id="mainNavigation" class="hasMainNavigationTheme_BlueSilver">
    <?php foreach ($links as $aSection): ?>
        <ul class="menu">
            <?php foreach ($aSection as $aLink): ?>
                <li class="item">
                    <?php echo link_to( $aLink['name'], "@{$aLink['route']}" . ( ($aLink['route'] == 'content') ? '?slug=' . $aLink['action'] : '') , array( 'class' => $aLink['is_selected'] ? 'current' : '' ) ); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endforeach; ?>
</div>