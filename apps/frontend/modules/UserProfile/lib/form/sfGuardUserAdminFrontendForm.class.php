<?php

/**
 * sfGuardUserAdminForm for admin generators
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUserAdminForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardUserAdminFrontendForm extends sfGuardUserAdminForm
{
  public function setup()
  {
      parent::setup();

      $this->useFields(array(
         'password',
         'password_again',
         'address1',
         'address2',
         'state',
         'city',
         'country',
         'zipcode',
         'receive_site_updates',
         'is_email_visible'
      ));
  }
}
