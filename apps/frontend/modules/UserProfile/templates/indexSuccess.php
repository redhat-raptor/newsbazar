<h1>Sf guard user profiles List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>User</th>
      <th>First name</th>
      <th>Last name</th>
      <th>Gender</th>
      <th>Date of birth</th>
      <th>Address1</th>
      <th>Address2</th>
      <th>State</th>
      <th>City</th>
      <th>Country</th>
      <th>Zipcode</th>
      <th>Email</th>
      <th>Keyboard</th>
      <th>Receive site updates</th>
      <th>Is email visible</th>
      <th>Validate</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($sf_guard_users as $sf_guard_user): ?>
    <tr>
      <td><a href="<?php echo url_for('user_profile_edit', $sf_guard_user) ?>"><?php echo $sf_guard_user->getId() ?></a></td>
      <td><?php echo $sf_guard_user ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('user_profile_new') ?>">New</a>
