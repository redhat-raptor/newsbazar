<?php use_helper('I18N') ?>

<?php if(!$sf_user->getGuardUser()): ?>
<div class="aLeftContent login hasSilverTheme">
    <h3 style="padding-left: 25px; padding-right: 25px;">Login</h3>
    <div class="cont">
        <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
            <?php echo $form->renderHiddenFields() ?>
            <ul>
                <li><?php echo $form['username']->render(array('class' => 'hasSingleBorder')) ?></li>
                <li><?php echo $form['password']->render(array('class' => 'hasSingleBorder')) ?></li>
            </ul>
            <p>
                <span class="remember_me">
                        <?php echo $form['remember']->render(array('class' => '')) ?>
                        <?php echo $form['remember']->renderLabel('remember') ?>
                </span>
                <span class="submit_login">
                    <input type="image" src="/images/themes/blue_silver/forward_arrow_transparent.png" alt="Login" />
                </span>
            </p>
        </form>
        <div class="clear-fix"></div>
    </div>    
</div>
<?php endif ?>