<div class="aLeftContent hasCremeYellowTheme hasMarginTop_15">
    <h3>Your Contact Details</h3>
    <div class="cont">
        <div class="block formContainer">
            <ul>
                <?php if($form->isNew()): ?>
                    <li>
                        <?php echo $form['email_address']->renderError() ?>
                        <?php echo $form['email_address']->renderLabel() ?>
                        <?php echo $form['email_address']->render() ?>
                    </li>
                <?php endif ?>
                <li>
                    <?php echo $form['address1']->renderError() ?>
                    <?php echo $form['address1']->renderLabel() ?>
                    <?php echo $form['address1']->render() ?>
                </li>
                <li>
                    <?php echo $form['address2']->renderError() ?>
                    <?php echo $form['address2']->renderLabel() ?>
                    <?php echo $form['address2']->render() ?>
                </li>
                <li>
                    <?php echo $form['country']->renderError() ?>
                    <?php echo $form['country']->renderLabel() ?>
                    <?php echo $form['country']->render() ?>
                </li>
                <li>
                    <?php echo $form['state']->renderError() ?>
                    <?php echo $form['state']->renderLabel() ?>
                    <?php echo $form['state']->render() ?>
                </li>
                <li>
                    <?php echo $form['city']->renderError() ?>
                    <?php echo $form['city']->renderLabel() ?>
                    <?php echo $form['city']->render() ?>
                </li>
                <li>
                    <?php echo $form['zipcode']->renderError() ?>
                    <?php echo $form['zipcode']->renderLabel() ?>
                    <?php echo $form['zipcode']->render() ?>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="aLeftContent hasCremeYellowTheme hasMarginTop_15">
    <h3>Preferences</h3>
    <div class="cont">
        <div class="block formContainer">
            <ul>
                <li>
                    <?php echo $form['receive_site_updates']->renderError() ?>
                    <?php echo $form['receive_site_updates']->renderLabel() ?>
                    <?php echo $form['receive_site_updates']->render() ?>
                </li>
                <li>
                    <?php echo $form['is_email_visible']->renderError() ?>
                    <?php echo $form['is_email_visible']->renderLabel() ?>
                    <?php echo $form['is_email_visible']->render() ?>
                </li>
            </ul>
        </div>
    </div>
</div>