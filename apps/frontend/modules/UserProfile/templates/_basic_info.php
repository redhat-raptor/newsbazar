<div class="aLeftContent hasCremeYellowTheme">
    <h3>Your Profile Information:</h3>
    <div class="cont">
        <div class="block formContainer">

            <ul>
                <li>
                    <label>First Name:</label>
                    <span class="input"><?php echo $form->getObject()->first_name ?></span>
                </li>
                <li>
                    <label>Last Name:</label>
                    <span class="input"><?php echo $form->getObject()->last_name ?></span>
                </li>
                <li>
                    <label>Gender:</label>
                    <span class="input"><?php echo $form->getObject()->gender ?></span>
                </li>
                <li>
                    <label>Username:</label>
                    <span class="input"><?php echo $form->getObject()->username ?></span>
                </li>
                <li>
                    <label>Email:</label>
                    <span class="input"><?php echo $form->getObject()->email_address ?></span>
                </li>
                <li>
                    <label>Date of Birth:</label>
                    <span class="input"><?php echo $form->getObject()->date_of_birth ?></span>
                </li>
            </ul>

        </div>
    </div>
</div>