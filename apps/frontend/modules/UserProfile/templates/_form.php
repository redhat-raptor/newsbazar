<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div id="mainContainerContent">
    <div class="block formContainer">
      <?php echo form_tag_for($form, '@user_profile') ?>
        <fieldset style="border: none">
            <?php echo $form->renderHiddenFields(false) ?>
            <?php echo $form->renderGlobalErrors() ?>
        </fieldset>

        <?php if($form->isNew()): ?>
          <?php include_partial('form_new', array('form' => $form)) ?>
        <?php else: ?>
          <?php include_partial('basic_info', array('form' => $form)) ?>
        <?php endif ?>
        <?php include_partial('form_edit', array('form' => $form)) ?>

        <div class="aLeftContent hasCremeYellowTheme hasMarginTop_15">
            <h3>Security</h3>
            <div class="cont">
                <div class="block formContainer">
                    <ul>
                        <?php if($form->isNew()): ?>
                            <li>
                                <?php echo $form['username']->renderError() ?>
                                <?php echo $form['username']->renderLabel() ?>
                                <?php echo $form['username']->render() ?>
                            </li>
                        <?php endif ?>
                        <li>
                            <?php echo $form['password']->renderError() ?>
                            <?php echo $form['password']->renderLabel() ?>
                            <?php echo $form['password']->render() ?>
                        </li>
                        <li>
                            <?php echo $form['password_again']->renderError() ?>
                            <?php echo $form['password_again']->renderLabel() ?>
                            <?php echo $form['password_again']->render() ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="block hasMarginTop_15 isFloated_Right">
          <input class="amButton hasAmButtonTheme_GreenOK" type="submit" value="<?php echo $form->isNew() ? 'Signup' : 'Update' ?>" />
        </div>
      </form>

      <div class="clear-fix"></div>

    </div>
</div>