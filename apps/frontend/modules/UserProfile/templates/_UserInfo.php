<?php if($sf_user->getGuardUser()): ?>
<div class="aRightContent hasSilverTheme">
    <h3>Hello <?php echo ucfirst($sf_user->getGuardUser()->username) ?></h3>
    <div class="cont">
        <ul>
            <li class="hasBackArrow hasBorderBottom">
                <?php echo link_to('news home', '@news', array('class' => 'link')) ?>
            </li>
        </ul>
        <p><?php echo link_to('Logout', '@sf_guard_signout', array('class' => 'link')) ?></p>
    </div>
</div>
<?php else: ?>
    <?php echo link_to('Register', '@user_profile_new', array('class' => 'tab link')) ?>
<?php endif ?>