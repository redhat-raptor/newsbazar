<div class="aLeftContent hasCremeYellowTheme">
    <h3>About you</h3>
    <div class="cont">
        <div class="block formContainer">

            <ul>
                <li>
                  <?php echo $form['first_name']->renderError() ?>
                  <?php echo $form['first_name']->renderLabel() ?>
                  <?php echo $form['first_name']->render() ?>
                </li>
                <li>
                  <?php echo $form['last_name']->renderError() ?>
                  <?php echo $form['last_name']->renderLabel() ?>
                  <?php echo $form['last_name']->render() ?>
                </li>
                <li>
                  <?php echo $form['gender']->renderError() ?>
                  <?php echo $form['gender']->renderLabel() ?>
                  <?php echo $form['gender']->render() ?>
                </li>
                <li>
                  <?php echo $form['date_of_birth']->renderError() ?>
                  <?php echo $form['date_of_birth']->renderLabel('Date of Birth') ?>
                  <?php echo $form['date_of_birth']->render() ?>
                </li>
            </ul>
        </div>
    </div>
</div>