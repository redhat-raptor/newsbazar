<?php slot('mainContainerHeader') ?>
    <h2>Create a new account</h2>
<?php end_slot() ?>

<?php include_partial('form', array('form' => $form)) ?>
