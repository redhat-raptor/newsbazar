<?php

/**
 * UserProfile actions.
 *
 * @package    ubn-sym
 * @subpackage UserProfile
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class UserProfileActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('UserProfile', 'edit');
  }

  public function executeNew(sfWebRequest $request)
  {
    if($this->getUser()->getGuardUser())
        $this->redirect('@user_profile');
    
    $this->form = new sfGuardRegisterForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    if($this->getUser()->getGuardUser())
        $this->redirect('@user_profile');
    
    $this->form = new sfGuardRegisterForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $user = $this->getUser();
    $this->form = new sfGuardUserAdminFrontendForm($user->getGuardUser(), array('user' => $user));
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $user = $this->getUser();
    $this->form = new sfGuardUserAdminFrontendForm($user->getGuardUser(), array('user' => $user));

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The Signup was completed successfully.' : 'Your profile was updated successfully.';
      $sf_guard_user_profile = $form->save();

      $this->getUser()->setFlash('notice', $notice);

      $this->getUser()->signin($sf_guard_user_profile);
      $this->redirect('@user_profile_edit?username='.$sf_guard_user_profile->username);
    }
  }
}