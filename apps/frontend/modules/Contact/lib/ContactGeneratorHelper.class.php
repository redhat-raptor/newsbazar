<?php

/**
 * Contact module helper.
 *
 * @package    ubn-sym
 * @subpackage Contact
 * @author     .
 * @version    SVN: $Id: helper.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactGeneratorHelper extends BaseContactGeneratorHelper
{
  public function linkToSave($object, $params)
  {
    return '<span class="action_list">
                <input class="amButton hasAmButtonTheme_GreenOK" type="submit" value="'.__($params['label'], array(), 'sf_admin').'" />
            </span>';
  }

  public function linkToList($params)
  {
    return '<span class="action_list">' .
                link_to(__($params['label'], array(), 'sf_admin'), '@homepage', array('class' => 'amButton hasAmButtonTheme_BlueList')) .
           '</span>';
  }
}
