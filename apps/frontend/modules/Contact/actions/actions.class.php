<?php

require_once dirname(__FILE__).'/../lib/ContactGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/ContactGeneratorHelper.class.php';

/**
 * Contact actions.
 *
 * @package    ubn-sym
 * @subpackage Contact
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactActions extends autoContactActions
{
  public function executeIndex(sfWebRequest $request)
  {
      $this->forward('Contact', 'new');
  }
  
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'Your message was sent successfully.' : 'The message was updated successfully.';

      try {
        $contact = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $contact)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@contact_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect('@homepage');
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
}
