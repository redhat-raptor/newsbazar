<?php use_helper('I18N', 'Date') ?>

<?php slot('mainContainerHeader') ?>
    <h2>Contact us</h2>
<?php end_slot() ?>

<div id="mainContainerContent">
    <?php include_partial('Contact/form', array('contact' => $contact, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
</div>