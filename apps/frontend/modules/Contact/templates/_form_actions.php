<div class="form_actions block hasMarginTop_15 ">
    <div class="nextActions">
        <?php echo $helper->linkToSave($form->getObject(), array( 'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Send',)) ?>
    </div>
    <div class="preActions">
        <?php echo $helper->linkToList(array(  'params' =>   array(  ),  'class_suffix' => 'list',  'label' => 'Back to Home',)) ?>
    </div>
</div>