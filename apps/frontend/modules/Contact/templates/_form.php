<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="aLeftContent hasCremeYellowTheme hasMarginTop_15 contactUs">
    <h3>Leave us a line</h3>
    <div class="cont">
        <div class="block formContainer">
            <?php echo form_tag_for($form, '@contact') ?>
                <?php echo $form->renderHiddenFields(false) ?>
                <ul>
                    <?php if ($form->hasGlobalErrors()): ?>
                        <?php echo $form->renderGlobalErrors() ?>
                    <?php endif; ?>

                    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
                        <?php include_partial('Contact/form_fieldset', array('contact' => $contact, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
                    <?php endforeach; ?>
                </ul>
                <?php include_partial('Contact/form_actions', array('contact' => $contact, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
            </form>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>      