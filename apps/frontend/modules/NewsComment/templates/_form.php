<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="aLeftContent hasCremeYellowTheme hasMarginTop_15">
    <h3>Write New Comment</h3>
    <div class="cont">
        <div class="block formContainer">
            <?php echo form_tag_for($form, '@news_comment') ?>
                <?php echo $form->renderGlobalErrors() ?>
                <?php echo $form->renderHiddenFields() ?>
                <ul>
                    <?php foreach($form->getFormFieldSchema() as $field): ?>
                        <?php if($field->isHidden()) continue ?>
                        <li>
                            <?php echo $field->renderError() ?>
                            <?php echo $field->renderLabel() ?>
                            <?php echo $field->render() ?>
                        </li>
                    <?php endforeach ?>
                </ul>

                <div class="form_actions block hasMarginTop_15">
                    <div class="nextActions">
                        <input class="amButton hasAmButtonTheme_GreenOK" type="submit" value="Submit" />
                    </div>
                </div>
            </form>
        </div>
        <div class="clear-fix"></div>
    </div>
</div>