<div id="mainContainerContent">
    <?php include_partial('NewsLocation/show', array('news' => $news)) ?>
    <?php include_component('NewsComment', 'NewsComment', array('news' => $news, 'form' => $form)) ?>
    
    <a href="<?php echo url_for('news') ?>">List</a>
    
    <div class="clear-fix"></div>
</div>