<h1>News comments List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Title</th>
      <th>Body</th>
      <th>Is published</th>
      <th>User</th>
      <th>News</th>
      <th>Parent</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($news_comments as $news_comment): ?>
    <tr>
      <td><a href="<?php echo url_for('NewsComment/edit?id='.$news_comment->getId()) ?>"><?php echo $news_comment->getId() ?></a></td>
      <td><?php echo $news_comment->getTitle() ?></td>
      <td><?php echo $news_comment->getBody() ?></td>
      <td><?php echo $news_comment->getIsPublished() ?></td>
      <td><?php echo $news_comment->getUserId() ?></td>
      <td><?php echo $news_comment->getNewsId() ?></td>
      <td><?php echo $news_comment->getParentId() ?></td>
      <td><?php echo $news_comment->getCreatedAt() ?></td>
      <td><?php echo $news_comment->getUpdatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('NewsComment/new') ?>">New</a>
