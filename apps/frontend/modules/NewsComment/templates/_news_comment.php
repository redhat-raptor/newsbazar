<?php $comments = $news->getComments();  ?>
<?php if(!count($comments)) return; ?>

<div class="aLeftContent hasCremeYellowTheme hasMarginTop_15">
    <h3>Comments</h3>
    <div class="cont">
        <?php foreach($comments as $aComment): ?>
            <div class="aComment">
                <div class="metas">
                    <p class="author-postTime">
                        by <span class="author"><?php echo $aComment->Author ?></span>
                        on <span class="postTime"><?php echo $aComment->getDateTimeObject('created_at')->format('d M, Y h:i:m') ?></span>
                    </p>
                    <span class="author-pointer"></span>
                    <h4 class="title"><?php echo $aComment->title ?></h4>
                </div>
                <div class="body">
                    <p><?php echo nl2br($aComment->body) ?></p>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>