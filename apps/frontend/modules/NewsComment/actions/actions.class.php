<?php

/**
 * NewsComment actions.
 *
 * @package    ubn-sym
 * @subpackage NewsComment
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsCommentActions extends sfActions
{
  public function executeCreate(sfWebRequest $request)
  {
    $news_comment_data = $request->getPostParameter('news_comment');
    $news_id = $news_comment_data['news_id'];
    $this->news = Doctrine::getTable('News')->find($news_id);

    $this->form = new NewsCommentForm(new NewsComment(), array('user' => $this->getUser()));
    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $form->getObject()->Author = $this->getUser()->getGuardUser();
      $news_comment = $form->save();

      //send an email to the admin
      $this->getMailer()->composeAndSend('info@newsbazar.com', 'ahsanulkc+newsbazar@gmail.com', 'Newsbazar contact us: ' . $news_comment->title, $news_comment->body);

      $this->getUser()->setFlash('notice', 'Your comment has been posted successfully!');

      $this->redirect('@news_show?id='.$news_comment->News->id);
    }
  }
}
