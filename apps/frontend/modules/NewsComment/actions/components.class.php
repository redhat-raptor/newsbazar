<?php

class NewsCommentComponents extends sfComponents
{
    public function executeNewsComment()
    {
        if(!$this->form) {
            $newsComment = new NewsComment();
            $newsComment->News = $this->news;
            $this->form = new NewsCommentForm($newsComment, array('user' => $this->getUser()));
        }
    }
}

?>
