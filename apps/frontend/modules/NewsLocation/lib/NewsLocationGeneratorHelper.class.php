<?php

/**
 * NewsLocation module helper.
 *
 * @package    ubn-sym
 * @subpackage NewsLocation
 * @author     .
 * @version    SVN: $Id: helper.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsLocationGeneratorHelper extends BaseNewsLocationGeneratorHelper
{
  public function linkToSave($object, $params)
  {
    return '<li class="action_list">
                <input class="amButton hasAmButtonTheme_GreenOK" type="submit" value="'.__($params['label'], array(), 'sf_admin').'" />
            </li>';
  }

  public function linkToList($params)
  {
    return '<li class="action_list">' .
                link_to(__($params['label'], array(), 'sf_admin'), '@'.$this->getUrlForAction('list'), array('class' => 'amButton hasAmButtonTheme_BlueList')) .
           '</li>';
  }
}
