<?php

require_once dirname(__FILE__).'/../lib/NewsLocationGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/NewsLocationGeneratorHelper.class.php';

/**
 * NewsLocation actions.
 *
 * @package    ubn-sym
 * @subpackage NewsLocation
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsLocationActions extends autoNewsLocationActions
{
  public function executeHome(sfWebRequest $request)
  {
    //set the location to global location
    $homeLocation = sfConfig::get('app_ubn_home_location_identifier');

    $this->setNecessaryFilter(Doctrine::getTable('NewsLocation')->createQuery('nl')->where('identifier = ?', $homeLocation)->fetchOne());
    
    //for the dropdown location selector in the header
    $this->setNewsLocationIdentifier($homeLocation);
      
    $this->forward('NewsLocation', 'index');
  }
  
  public function executeIndexLocation(sfWebRequest $request)
  {
      $this->newsLocationList = $this->getLocation();

      if(!array_key_exists('actual', $this->newsLocationList))
        return 'Recommended';

      $this->location = $this->newsLocationList['actual'][0];
      
      $this->setNecessaryFilter($this->location);

      //for the dropdown location selector in the header
      $this->setNewsLocationIdentifier($this->location->identifier);

      $this->forward('NewsLocation', 'index');
  }
  
  private function setNewsLocationIdentifier($identifier)
  {
    //for the dropdown location selector in the header
    $this->getUser()->setAttribute('news_location_identifier', $identifier);
  }

  private function getNewsLocationIdentifier()
  {
    $homeLocation = sfConfig::get('app_ubn_home_location_identifier');
    //for the dropdown location selector in the header
    return $this->getUser()->getAttribute('news_location_identifier', $homeLocation);
  }
  
  public function executeIndex(sfWebRequest $request)
  {
    $this->location = $this->getLocationFromFilter();

    // sorting
    if ($request->getParameter('sort') && $this->isValidSortColumn($request->getParameter('sort')))
    {
      $this->setSort(array($request->getParameter('sort'), $request->getParameter('sort_type')));
    }

    // pager
    if ($request->getParameter('page'))
    {
      $this->setPage($request->getParameter('page'));
    }

    $this->pager = $this->getPager();
    $this->sort = $this->getSort();
  }

  public function executeNew(sfWebRequest $request)
  {
    $currentLocationIdentifier = $this->getNewsLocationIdentifier();
    $this->form = new NewsForm(new News(), array(
      'user' => $this->getUser(),
      'location_identifier' => $currentLocationIdentifier
    ));
    
    $this->news = $this->form->getObject();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->form = new NewsForm(new News(), array('user' => $this->getUser()));
    $this->news = $this->form->getObject();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }
  
  public function executeShow(sfWebRequest $request)
  {
    $this->news = $this->getRoute()->getObject();
    
    //for the dropdown location selector in the header
    $this->setNewsLocationIdentifier($this->news->Location->identifier);
  }
  
  private function setNecessaryFilter($location)
  {
    $filters = $this->getFilters();
    $filters['location_id'] = $location->id;

    $this->setFilters($filters);
  }

  private function getLocationFromFilter()
  {
      $filters = $this->getFilters();
      if(isset($filters['location_id']))
          return Doctrine::getTable('NewsLocation')->find($filters['location_id']);

      return null;
  }

  private function getLocation()
  {
      /*******************************************************************
       * checking if the user has a valid news location
       ******************************************************************/
      $reqPath = strtolower($this->request->getPathInfo());
      
      //If there is no location set in the url, the user is given the default location
      if($reqPath == '/')
        return array(
          'actual' => array(Doctrine::getTable('NewsLocation')->findOneBy('identifier', sfConfig::get('app_ubn_home_location_identifier'))
        ));  
      
      
      $pathList = explode('/', $reqPath);

      //we don need the first two
      unset($pathList[0]);
      unset($pathList[1]);

      if(!count($pathList)) {
          return array();
      }

      $lastElement = count($pathList) + 2 - 1; //as we unset 2 initial elements

      return Doctrine::getTable('NewsLocation')->getPath($pathList[$lastElement], $pathList);
  }

  private function getActualLocation()
  {
      $locationList = $this->getLocation();
      if(array_key_exists('actual', $locationList))
        return $locationList['actual'][0];

      return null;
  }

  public function buildQuery()
  {
    $tableMethod = $this->configuration->getTableMethod();
    if (null === $this->filters)
    {
      $this->filters = $this->configuration->getFilterForm($this->getFilters());
    }

    $this->filters->setTableMethod($tableMethod);

    $query = $this->filters->buildQuery($this->getFilters());

    $actualLocation = $this->getActualLocation();

    /*******************************************************************
    * Adding order by first by created_at and then by priority
    * Cascaded News
    ******************************************************************/
    if($actualLocation)
        $query = Doctrine::getTable('News')->getQueryNewsCascaded($actualLocation, $query);

    $query->addWhere('is_published = ?', true);
    $this->addSortQuery($query);

    $event = $this->dispatcher->filter(new sfEvent($this, 'admin.build_query'), $query);
    $query = $event->getReturnValue();

    return $query;
  }
  
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $form->getObject()->user_id = $this->getUser()->getGuardUser()->id;
        $news = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $news)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@news_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        $this->redirect(array('sf_route' => 'news_show', 'sf_subject' => $news));
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
  
}
