<?php

class NewsLocationComponents extends sfComponents
{
    public function executeNewsLocationSelector()
    {
        $this->form = new NewsLocationSelectorForm();
        $selectedLocationIdentifier = $this->getUser()->getAttribute('news_location_identifier', sfConfig::get('app_ubn_home_location_identifier'));
        
        $selectedLocation = Doctrine::getTable('NewsLocation')->findOneBy('identifier', $selectedLocationIdentifier);

        if(is_object($selectedLocation) && $selectedLocation->id)
            $this->form->setDefault('location_id', $selectedLocation->id);
    }
    
    public function executeFeaturedNews()
    {
        $selectedLocationIdentifier = $this->getUser()->getAttribute('news_location_identifier', sfConfig::get('app_ubn_home_location_identifier'));
        
        $selectedLocation = Doctrine::getTable('NewsLocation')->findOneBy('identifier', $selectedLocationIdentifier);
        
        if(is_object($selectedLocation) && $selectedLocation->id) {
            $this->featuredNews = Doctrine::getTable('News')->getFeaturedNews($selectedLocation);
        }
        else
            return;
    }
}

?>