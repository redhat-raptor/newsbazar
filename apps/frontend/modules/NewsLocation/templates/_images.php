<div class="gallery">
    <ul>
        <?php foreach($news->Images as $anImage): ?>
            <li>
                <a title="" href="#" onclick="return false;">
                    <?php echo ubn_image_lightbox($anImage, array(), 72, 72) ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
</div>