<div class="sf_admin_list">
  <?php if (!$pager->getNbResults()): ?>

    <?php slot('mainContainerNotice') ?>
        <p class="notice hasNoticeTheme_Yellow"><?php echo __('Sorry! No News Added Yet...', array(), 'sf_admin') ?></p>
    <?php end_slot() ?>

  <?php else: ?>
        <?php foreach ($pager->getResults() as $i => $news): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
          <?php include_partial('NewsLocation/list_td_tabular', array('news' => $news)) ?>
        <?php endforeach; ?>
        
        <?php if ($pager->haveToPaginate()): ?>
          <?php include_partial('NewsLocation/pagination', array('pager' => $pager)) ?>
        <?php endif; ?>

        <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
        <?php if ($pager->haveToPaginate()): ?>
          <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
        <?php endif; ?>
  <?php endif; ?>
</div>
