<div class="aNews hasANewsTheme_OrangeWhite">
  <h3 class="title">
    <?php echo $news->getTitle() ?>
  </h3>
  <p class="metas">
    by <span class="author"><?php echo $news->Author ?></span>
    on <span class="postTime"><?php echo $news->getDateTimeObject('created_at')->format('d M, Y h:i:m') ?></span>
  </p>
  <div class="body">
    <p>
      <?php echo nl2br($news->getBody()) ?>
    </p>
  </div>
  <div class="actions">
  </div>
    <?php include_partial('NewsLocation/images', array('news' => $news)) ?>
</div>