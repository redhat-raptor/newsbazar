<?php $pathList =  $location->getPathList() ?>

<ol class="browser-path">
    <?php foreach($pathList as $aLocationPath): ?>
        <?php echo link_to_location("<span>$aLocationPath</span><b></b>", $aLocationPath->getPathURI()) ?>
    <?php endforeach ?>
</ol>