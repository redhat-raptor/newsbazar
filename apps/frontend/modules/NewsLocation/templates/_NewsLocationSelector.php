<?php
    echo $form['location_id']->render(array(
        'class' => 'centeredContainer block locationSelector',
        'onchange' => 'changeLocation(this.value)'
    ));
?>

<script type="text/javascript">
    function changeLocation(location)
    {
        var locationURL = "<?php echo url_for('@news_location_path')?>";
        window.location = locationURL + '/' + location;
    }
</script>