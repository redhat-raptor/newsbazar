<?php use_helper('I18N', 'Date') ?>

<?php slot('mainContainerBreadcrumb') ?>
    <?php include_partial('NewsLocation/location_breadcrumb', array('location' => $location)) ?>
<?php end_slot() ?>

<?php slot('mainContainerHeader') ?>
    <h2>News Index of <?php echo $location ?></h2>
<?php end_slot() ?>

<?php include_partial('NewsLocation/flashes') ?>

<div id="mainContainerListHeader">
    <?php include_partial('NewsLocation/list_header', array('pager' => $pager)) ?>
</div>

<?php include_component('NewsLocation', 'FeaturedNews') ?>

<div id="mainContainerSearch">
    <!-- temporarily disabling searching options -->
    <?php //include_partial('NewsLocation/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
</div>

<div id="mainContainerContent">
    <div id="mainCont" class="<?php echo count($location->Advertisements) ? 'hasAdvert' : '' ?>">
        <?php include_partial('NewsLocation/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    </div>
    
    <?php if(count($location->Advertisements)): ?>
        <div class="anAdvert">
            <?php foreach($location->Advertisements as $anAdvertisement): ?>
                <?php echo ubn_image_tag($anAdvertisement, array(), 150, 200) ?>
            <?php endforeach ?>
        </div>
    <?php endif ?>
    
    <div class="clear-fix"></div>
</div>

<div id="mainContainerFooter">
    <?php include_partial('NewsLocation/list_footer', array('pager' => $pager)) ?>
</div>