<div class="newsContainer hasNewsContainerTheme_OrangeWhite">
    <h3><?php echo link_to($news->getTitle(), 'news_show', $news) ?></h3>
    <div class="newsDetails">
        <p class="newsInfo">
            By <span><?php echo $news->getAuthor() ?></span>
            on <?php echo false !== strtotime($news->getCreatedAt()) ? format_date($news->getCreatedAt(), "f") : '&nbsp;' ?>
        </p>
        <p class="newsBody">
            <?php echo nl2br($news->getBodyShort()) ?>
        </p>
    </div>
</div>