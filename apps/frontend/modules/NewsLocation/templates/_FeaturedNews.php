<div class="Slider_mainContainer">
    <div class="Slider_Content">
        <div class="Slider_slider">
            <ul>
                <?php $featuredNewsTitleList = '' ?>
                <?php foreach($featuredNews as $aFeaturedNews): ?>
                    <?php $newsRoute = '@news_show?id='.$aFeaturedNews->id; ?>
                    <li>
                        <h2><?php echo link_to($aFeaturedNews->title, $newsRoute) ?></h2>
                        <p>
                            <?php if($image = $aFeaturedNews->getRandomImage()): ?>
                                <?php echo ubn_image_link($image, $newsRoute, array(), 240, 200, 'dce5e9') ?>
                            <?php endif ?>

                            <?php echo $aFeaturedNews->body ?>
                        </p>
                    </li>
                    <?php $featuredNewsTitleList[] = "'" . $aFeaturedNews->title . "'"?>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
      $(document).ready(function(){	
        $(".Slider_slider").easySlider({
          auto: true,
          continuous: true,
          string: true,
          stringList: new Array(
            <?php echo implode(',', $featuredNewsTitleList) ?>
          ),
          speed: 1500
        });
      });
    });
</script>