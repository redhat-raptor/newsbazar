<?php use_helper('I18N', 'Date') ?>

<?php slot('mainContainerHeader') ?>
    <h2>New News</h2>
<?php end_slot() ?>

<div id="mainContainerContent">
    <?php include_partial('NewsLocation/form', array('news' => $news, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
</div>