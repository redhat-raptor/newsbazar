<?php use_helper('I18N', 'Date') ?>

<?php slot('mainContainerHeader') ?>
    <h2>Opps! We could not find the location!</h2>
<?php end_slot() ?>

<div id="mainContainerContent">
    <?php if(isset($newsLocationList['recommended'])): ?>

        <?php slot('mainContainerNotice') ?>
            <p class="notice hasNoticeTheme_Yellow"><?php echo __('However below is a list of location(s) that matched our record:', array(), 'sf_admin') ?></p>
        <?php end_slot() ?>

        <?php foreach($newsLocationList['recommended'] as $aRecommendedLocation): ?>
            <?php include_partial('NewsLocation/location_info', array('location' => $aRecommendedLocation)) ?>
        <?php endforeach ?>
    <?php endif ?>
</div>