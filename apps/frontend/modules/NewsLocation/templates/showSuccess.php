<?php use_helper('I18N', 'Date') ?>

<?php slot('mainContainerBreadcrumb') ?>
    <?php include_partial('NewsLocation/location_breadcrumb', array('location' => $news->getLocation())) ?>
<?php end_slot() ?>

<div id="mainContainerContent">
  <?php include_partial('NewsLocation/show', array('news' => $news)) ?>
  <?php include_component('NewsComment', 'NewsComment', array('news' => $news)) ?>

  <a class="amButton hasAmButtonTheme_BlueList hasMarginTop_15 isFloated_Left" href="<?php echo url_for('news') ?>">Back to <?php echo $news->Location ?> News List</a>
  <div class="clear-fix"></div>
</div>