<div class="form_actions block hasMarginTop_15">
    <div class="preActions">
        <?php echo $helper->linkToList(array(  'params' =>   array(  ),  'class_suffix' => 'list',  'label' => 'Back to list',)) ?>
    </div>

    <div class="nextActions">
        <?php echo $helper->linkToSave($form->getObject(), array( 'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
    </div>
</div>