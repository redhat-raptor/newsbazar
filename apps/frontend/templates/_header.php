<div id="headerBar" class="hasHeaderBarTheme_BlueSilver"></div>
<div id="header" class="hasHeaderTheme_BlueSilver">
	<div id="header-inner" class="clear-block centeredContainer fullWidth">
            <div id="login">
                <?php include_component('UserProfile','UserLogin') ?>
            </div>
            <div id="logo-title">
                <h1 id="site-name">
                    <?php echo link_to(image_tag('logo', array('alt' => 'News Bazar')), '@homepage') ?>
                </h1>
                <?php include_component('NewsLocation', 'NewsLocationSelector') ?>
            </div>
            <div id="userInfo">
                <?php include_component('UserProfile','UserInfo') ?>
            </div>
	</div>
</div>