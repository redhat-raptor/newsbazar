<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body class="hasBodyTheme_BlueSilver">
  	<?php include_partial('global/header') ?>
        <div id="mainContainer" class="hasMainContainerTheme_BlueSilver">
            <div id="mainContainerInner" class="hasMainContainerInnerTheme_BlueSilver">
                <div id="mainContainerInnerContent" class="hasMainContainerInnerContentTheme_BlueSilver">
                    <?php include_component('home', 'MainNavigation') ?>

                    <div id="container" class="hasContainerTheme_BlueSilver">

                        <?php if(has_slot('mainContainerBreadcrumb')): ?>
                            <div class="mainContainerHeader">
                                <?php include_slot('mainContainerBreadcrumb') ?>
                            </div>
                        <?php endif ?>
                        
                        <?php if(has_slot('mainContainerHeader')): ?>
                            <div class="mainContainerHeader">
                                <?php include_slot('mainContainerHeader') ?>
                            </div>
                        <?php endif ?>

                        <?php if(has_slot('mainContainerNotice')): ?>
                            <div class="mainContainerHeader">
                                <?php include_slot('mainContainerNotice') ?>
                            </div>
                        <?php endif ?>

                        <?php echo $sf_content ?>
                    </div>
                </div>
            </div>
        </div>
	<script type="text/javascript">
	  $(function() {
	    /* jCarouselLite does not work reliably
	    $(".gallery").jCarouselLite({
	      btnNext: ".next",
	      btnPrev: ".prev"
	    });
	    */
	    $('.lightbox').lightBox({fixedNavigation:true});
	  });
	</script>
	
  </body>
</html>