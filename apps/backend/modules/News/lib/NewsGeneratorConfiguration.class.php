<?php

/**
 * News module configuration.
 *
 * @package    ubn-sym
 * @subpackage News
 * @author     .
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsGeneratorConfiguration extends BaseNewsGeneratorConfiguration
{
  public function getFormOptions()
  {
    return array('user' => sfContext::getInstance()->getUser());
  }
}
