<?php

require_once dirname(__FILE__).'/../lib/NewsCommentGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/NewsCommentGeneratorHelper.class.php';

/**
 * NewsComment actions.
 *
 * @package    ubn-sym
 * @subpackage NewsComment
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsCommentActions extends autoNewsCommentActions
{
}
