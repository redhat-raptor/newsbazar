<?php

require_once dirname(__FILE__).'/../lib/ContactGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/ContactGeneratorHelper.class.php';

/**
 * Contact actions.
 *
 * @package    ubn-sym
 * @subpackage Contact
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactActions extends autoContactActions
{
}
