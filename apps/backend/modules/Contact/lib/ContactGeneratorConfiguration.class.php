<?php

/**
 * Contact module configuration.
 *
 * @package    ubn-sym
 * @subpackage Contact
 * @author     .
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactGeneratorConfiguration extends BaseContactGeneratorConfiguration
{
  public function getForm($object = null, $options = array())
  {
    $class = $this->getFormClass();
    $options = array_merge($options, array(
      'user' => sfContext::getInstance()->getUser()
    ));
    return new $class($object, array_merge($this->getFormOptions(), $options));
  }
}
