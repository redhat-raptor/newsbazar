<?php

require_once dirname(__FILE__).'/../lib/AdvertisementGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/AdvertisementGeneratorHelper.class.php';

/**
 * Advertisement actions.
 *
 * @package    ubn-sym
 * @subpackage Advertisement
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertisementActions extends autoAdvertisementActions
{
}
