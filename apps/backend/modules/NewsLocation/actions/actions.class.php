<?php

require_once dirname(__FILE__).'/../lib/NewsLocationGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/NewsLocationGeneratorHelper.class.php';

/**
 * NewsLocation actions.
 *
 * @package    ubn-sym
 * @subpackage NewsLocation
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsLocationActions extends autoNewsLocationActions
{
  protected function addSortQuery($query)
  {
    $query->addOrderBy('root_id asc');
    $query->addOrderBy('lft asc');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->getNode()->delete())
    {
      $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('@news_location');
  }

  protected function executeBatchDelete(sfWebRequest $request)
  {
    $ids = $request->getParameter('ids');
    $records = Doctrine_Query::create()
        ->from('NewsLocation')
        ->whereIn('id', $ids)
        ->execute();
    foreach ($records as $record)
    {
      $record->getNode()->delete();
    }

    $this->getUser()->setFlash('notice', 'The selected items have been deleted successfully.');
    $this->redirect('@news_location');
  }
}
