<?php

require_once dirname(__FILE__).'/../lib/NewsImageGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/NewsImageGeneratorHelper.class.php';

/**
 * NewsImage actions.
 *
 * @package    ubn-sym
 * @subpackage NewsImage
 * @author     .
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsImageActions extends autoNewsImageActions
{
  public function executeAjaxNew(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $ajax_embedHash = $request->getParameter('ajax_embed_hash');

    $ajax_embedList = explode('--', $ajax_embedHash);
    $ajax_embedTree = array();
    foreach ($ajax_embedList as $key => $values)
      $ajax_embedTree[] = explode('-', $values);

    $ajax_module = 'NewsImage';
    $ajax_formClass = $ajax_embedTree[0][0];

    $ajax_index = $this->getUser()->getAttribute($ajax_module, 1, 'ubn/AjaxFormIndex');
    $this->getUser()->setAttribute($ajax_module, $ajax_index + 1, 'ubn/AjaxFormIndex');

    $parentForm = new $ajax_formClass();
    $parentName = $parentForm->getName();
    $ajaxIndexList = $parentForm->embedRelationTree("new{$ajax_index}", $ajax_embedHash, $ajax_embedTree);

    $ajaxForm = $parentForm;
    foreach ($ajaxIndexList as $ajaxIndex)
      $ajaxForm = $ajaxForm[$ajaxIndex];

    return $this->renderText($ajaxForm->renderRow());
  }
}
