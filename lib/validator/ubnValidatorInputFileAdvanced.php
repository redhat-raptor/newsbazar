<?php

class ubnValidatorInputFileAdvanced extends sfValidatorBase
{
  protected function configure($options = array(), $messages = array())
  {
    $this->addOption('path', null);
    parent::configure();
  }

  public function doClean($value)
  {
    $this->tmpDir = sfConfig::get('sf_root_dir') . '/data/ubnAdvancedUpload';
    $this->tmpFileName = $this->tmpDir . '/' . $value;

    $this->newDir = sfConfig::get('sf_web_dir'). '/' .$this->getOption('path');
    $this->newFileName =  $this->newDir . '/' . $value;

    if (!is_dir($this->newDir)) {
        $old_umask = umask(0);
        mkdir($this->newDir, 0777);
        umask($old_umask);
    }
    if (!is_writable($this->newDir)) {
      if (!chmod($this->newDir, 0777))
        throw new sfException();
    }

    if (file_exists($this->tmpFileName) && is_dir($this->newDir)) {
      if (copy($this->tmpFileName, $this->newFileName)) {
        unlink($this->tmpFileName);
        chmod($this->newFileName, 0666);
      }
    }
    
    return $value;
  }
}