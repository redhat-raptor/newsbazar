<?php

class sfWidgetFormSchemaFormatterMgtsTable extends sfWidgetFormSchemaFormatterTable
{
  protected
    $rowFormat = '
      <tr>
        <th>%label%</th>
        <td>
          %error%
          %field%
          %help%
          %hidden_fields%
        </td>
      </tr>
    ',
    $errorRowFormat = '
      <tr>
        <td colspan="2">%errors%</td>
      </tr>
    ',
    $helpFormat  = '
      %help%
    ',
    $decoratorFormat = '
      <table>
        <tbody>%content%</tbody>
      </table>
    ';

  protected
    $requiredFormat = '
      <em class="required_form_item">*</em>
    ';
    
  protected
    $validatorSchema = null;

  /** 
   * Generates the label name for the given field name. 
   * 
   * @param  string $name  The field name 
   * @return string The label name 
   */
  public function generateLabelName($name)
  {
    $label = $this->widgetSchema->getLabel($name);

    if (!$label && false !== $label)
    {
      if (substr($name, -3) == '_id')
        $label = substr($name, 0, -3);
      else if (substr($name, -5) == '_list')
        $label = substr($name, 0, -5);
      else
        $label = $name;

      $label = str_replace('_', ' ', $label);
      $label = ucwords($label);
    }

    if ($this->validatorSchema) {
      $fields = $this->validatorSchema->getFields();
      if ($fields[$name] && $fields[$name]->hasOption('required') && $fields[$name]->getOption('required'))
        $label = $label . $this->requiredFormat;
    }

    return $this->translate($label);
  }

  public function setValidatorSchema(sfValidatorSchema $validatorSchema)
  {
    $this->validatorSchema = $validatorSchema;
  }
  
  public function setDecoratorId($id)
  {
    $this->decoratorFormat =
<<<HTML
      <table id="{$id}">
        <tbody>%content%</tbody>
      </table>
HTML;
  }
}
