<?php

class amWidgetFormSchemaFormatterList extends sfWidgetFormSchemaFormatterList
{
  protected
    $requiredTemplate= '<em class="required_form_item">*</em>',
    $validatorSchema = null;

  /**
   * Generates the label name for the given field name.
   *
   * @param  string $name  The field name
   * @return string The label name
   */
  public function generateLabelName($name)
  {
    $label = parent::generateLabelName($name);

    if ($this->validatorSchema) {
        $fields = $this->validatorSchema->getFields();
        if ($fields[$name] && $fields[$name]->hasOption('required') && $fields[$name]->getOption('required'))
            $label = $this->requiredTemplate . $label;
    }

    return $label;
  }

  public function setValidatorSchema(sfValidatorSchema $validatorSchema)
  {
    $this->validatorSchema = $validatorSchema;
  }
}
