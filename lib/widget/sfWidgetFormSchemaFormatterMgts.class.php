<?php

class sfWidgetFormSchemaFormatterMgts extends sfWidgetFormSchemaFormatterMgtsTable
{
  protected
    $rowFormat = '
      <li class="form_list_item">
        <div class="label_box">
          %label%
        </div>
        <div class="inp_box">
          %error%
          %field%
          %help%
          %hidden_fields%
        </div>
      </li>
    ',
    $errorRowFormat = '
      <div class="error">
        %errors%
      </div>
    ',
    $helpFormat  = '
      <div class="help">
        %help%
      </div class="help">
    ',
    $decoratorFormat = '
      <ul class="form_list">
        %content%
      </ul>
    ';

  public function setDecoratorId($id)
  {
    $this->decoratorFormat =
<<<HTML
      <ul class="form_list" id="{$id}">
        %content%
      </ul>
HTML;
  }
}
