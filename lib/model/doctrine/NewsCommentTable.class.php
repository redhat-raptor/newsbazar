<?php

class NewsCommentTable extends CommentTable
{
    public function getComments(News $news)
    {
        return $this->createQuery()
                ->addWhere('news_id = ?', $news->id)
                ->addOrderBy('created_at DESC')
                ->execute();
    }
}