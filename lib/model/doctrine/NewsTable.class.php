<?php

class NewsTable extends CommentTable
{
    public function getFeaturedNews(NewsLocation $location)
    {
        $locations = $location->getNode()->getDescendants();
        $locations[] = $location;

        $locationIds = array();
        foreach($locations as $aLocation)
            $locationIds[] = $aLocation->id;

        $q = $this->createQuery();
        $q->addWhere('is_published = ?', true);
        $q->whereIn('location_id', $locationIds);
        $q->addOrderBy('priority DESC');
        $q->addOrderBy('created_at DESC');
        $q->limit(sfConfig::get('app_max_featured_news', 4));

        $result = $q->execute();

        return $result;
    }

    public function getQueryNewsCascaded(NewsLocation $location, $q)
    {
        $locations = $location->getNode()->getDescendants();
        $locations[] = $location;

        $locationIds = array();
        foreach($locations as $aLocation) {
            $locationIds[] = $aLocation->id;
        }

        $q->orWhereIn('location_id', $locationIds);
        $q->addOrderBy('priority DESC');
        $q->addOrderBy('created_at DESC');
        $q->addOrderBy('location_id');

        return $q;
    }
}
