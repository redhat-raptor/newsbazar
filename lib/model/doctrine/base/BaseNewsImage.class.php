<?php

/**
 * BaseNewsImage
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $news_id
 * @property boolean $is_featured
 * @property News $News
 * 
 * @method integer   getNewsId()      Returns the current record's "news_id" value
 * @method boolean   getIsFeatured()  Returns the current record's "is_featured" value
 * @method News      getNews()        Returns the current record's "News" value
 * @method NewsImage setNewsId()      Sets the current record's "news_id" value
 * @method NewsImage setIsFeatured()  Sets the current record's "is_featured" value
 * @method NewsImage setNews()        Sets the current record's "News" value
 * 
 * @package    ubn-sym
 * @subpackage model
 * @author     .
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseNewsImage extends Image
{
    public function setTableDefinition()
    {
        parent::setTableDefinition();
        $this->setTableName('news_image');
        $this->hasColumn('news_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('is_featured', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));

        $this->option('collate', 'utf8_unicode_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('News', array(
             'local' => 'news_id',
             'foreign' => 'id',
             'cascade' => array(
             0 => 'delete',
             )));
    }
}