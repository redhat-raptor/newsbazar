<?php

class NewsLocationTable extends Doctrine_Table
{
    public function getPath($locationName, $locationPath)
    {
        $locationPath = array_reverse($locationPath);
        
        $newsLocations = $this->createQuery('nl')->where('identifier = ?', $locationName)->execute();

        $validLocationList = array();

        foreach($newsLocations as $aNewsLocation)
        {
            $currentLocationPath = $aNewsLocation->getPathList('getIdentifier'); //get array of unique location identifiers instead of objects

            if(!count(array_diff($currentLocationPath, $locationPath))) //found the one looking for
                $validLocationList['actual'][] = $aNewsLocation;
            else //suggest locations
                $validLocationList['recommended'][] = $aNewsLocation;
        }

        return $validLocationList;
    }
    
    public function getLocationByIdentifier($identifier)
    {
        return $this->createQuery('nl')->where('identifier = ?', $identifier)->fetchOne();
    }
}