<?php

abstract class UBNBaseFormDoctrine extends UBNEmbeddableFormDoctrine
{
  protected $formId = '';

  public function setup()
  {
    parent::setup();

    $this->configureId();
    $this->configureFormatter();
    $this->configureFileFields();
  }

  public function unsetFields(array $fieldArray)
  {
    foreach ($fieldArray as $field)
      if (isset($this[$field]))
        unset($this[$field]);
  }
  
  // Apply our own formatter for forms, including * for required fields
  protected function configureFormatter()
  {
    $this->widgetSchema->setFormFormatterName('Mgts');
    if ($formatter = $this->widgetSchema->getFormFormatter())
      $formatter->setValidatorSchema($this->getValidatorSchema());    
  }
  
  // Set the correct widgets if it is an *ImageForm or *DocumentForm
  protected function configureFileFields()
  {
    if (is_subclass_of($this->getObject(), 'Image'))
    {
      $this->widgetSchema['filename'] = new sfWidgetFormInputFileEditable(array(
        'label'     => 'Image',
        //'file_src'  => ubn_image_link($this->object, array('class' => 'edit-view-thumb'), 320, 320),
        'file_src'  => '',
        'is_image'  => true,
        'edit_mode' => !$this->isNew(),
        'template'  => '<div>%file%<br />%input%<br />%delete% %delete_label%</div>',
      ));
      
      $this->validatorSchema['filename'] = new sfValidatorFile(array(
        'path' => UBNHelper::getUploadPath(),
        'required' => true
      ));
    }
  }
  
  protected function configureId()
  {
    $this->formId = uniqid('ubnForm');
  }
}
