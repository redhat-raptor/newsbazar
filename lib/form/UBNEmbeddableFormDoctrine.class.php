<?php

abstract class UBNEmbeddableFormDoctrine extends sfFormDoctrine
{
  public function setup()
  {
    // Load commonly used helpers in forms
    sfApplicationConfiguration::getActive()->loadHelpers(array('Asset', 'Tag', 'Url', 'Date', 'Partial', 'jQuery'));

    if (!isset($this->options['embed_tree']))
      $this->options['embed_tree'] = array();
  }

  // Always make multipart (in case image forms are ajax embedded)
  public function isMultipart()
  {
    return true;
  }

  public function embedRelationForm($relationName, $foreignRelationName, $formClass = null, $options = array())
  {
    $table = $this->object->getTable();
    if (!$table->hasRelation($relationName))
      throw new RuntimeException(sprintf('Relation "%s" does not exist on table "%s"', $relationName, $table->getTableName()));

    $relation = $table->getRelation($relationName);
    //$this->object->refreshRelated($relationName); // Dunno why I originally had this, but I cannot keep it

    $objectClass = $relation->getClass();

    $parentFormClass = get_class($this);
    $formClass = is_null($formClass) ? $relation->getClass() . 'Form' : $formClass;
    if (!class_exists($formClass))
      throw new RuntimeException(sprintf('Form of class "%s" does not exist', $formClass));

    $options = sfForm::deepArrayUnion($this->options, $options);
    $options['embed_tree'][] = array($parentFormClass, $relationName, $foreignRelationName, $formClass);
    $embedOptions = isset($options[$this->getModelName()]) ? $options[$this->getModelName()] : array();

    $label = isset($embedOptions['label']) ? $embedOptions['label'] : $relationName;

    if ($relation->isOneToOne())
    {
      /*
      if (!$this->object[$relationName]->exists())
        $this->object[$relationName][$foreignRelationName] = $this->object;
      */

      $form = new $formClass($this->object[$relationName], $options);
      unset($form[$relation->getForeign()]);

      $ajaxEmbedList = array();
      $ajaxEmbedIdList = array();
      foreach ($options['embed_tree'] as $key => $values) {
        $ajaxEmbedList[] = implode('-', $values);
        $ajaxEmbedIdList[] = $values[1];
        if (isset($values[4]))
          $ajaxEmbedIdList[] = $values[4];
      }
      $ajaxEmbedHash = implode('--', $ajaxEmbedList);
      $ajaxEmbedId = implode('_', $ajaxEmbedIdList);

      $formId = $this->widgetSchema->generateId("{$ajaxEmbedId}_ONE[]");
      if ($formatter = $form->widgetSchema->getFormFormatter())
        $formatter->setDecoratorId($formId);

      $this->embedForm("{$relationName}_ONE", $form);
      $this->widgetSchema->setLabel("{$relationName}_ONE", $label);

      ///*
      if (isset($this[$relation->getLocal()]))
      {
        // replace the current form element with the new embedded form
        $this->widgetSchema->moveField("{$relationName}_ONE", sfWidgetFormSchema::AFTER, $relation->getLocal());
        $this->getWidget($relation->getLocal())->setAttribute('disabled', true);
        unset($this[$relation->getLocal()]);
      }
      //*/
    }
    else
    {
      $ajaxModule = isset($embedOptions['ajax_module']) ? $embedOptions['ajax_module'] : $relation->getClass();
      $ajaxActionNew = isset($embedOptions['ajax_action_new']) ? $embedOptions['ajax_action_new'] : 'ajaxNew';
      $ajaxActionDelete = isset($embedOptions['ajax_action_delete']) ? $embedOptions['ajax_action_delete'] : 'ajaxDelete';

      $subForm = new BaseForm();

      $ajaxEmbedList = array();
      $ajaxEmbedIdList = array();
      foreach ($options['embed_tree'] as $key => $values) {
        $ajaxEmbedList[] = implode('-', $values);
        $ajaxEmbedIdList[] = $values[1];
        if (isset($values[4]))
          $ajaxEmbedIdList[] = $values[4];
      }
      $ajaxEmbedHash = implode('--', $ajaxEmbedList);
      $ajaxEmbedId = implode('_', $ajaxEmbedIdList);

      $subFormId = $this->widgetSchema->generateId("{$ajaxEmbedId}_MANY[]");

      $subForm->widgetSchema->setFormFormatterName('Mgts');
      if ($formatter = $subForm->widgetSchema->getFormFormatter())
        $formatter->setDecoratorId($subFormId);

      $subForm->setWidget('ajax_new', new sfWidgetFormInput(array('type' => 'hidden'), array('disabled' => true)));
      $subForm->setValidator('ajax_new', new sfValidatorString(array('required' => false)));

      $subForm->widgetSchema->setLabel('ajax_new', '&nbsp;');
      $subForm->widgetSchema->setHelp('ajax_new',
        jq_button_to_remote("Add new", array(
          'url'      => "{$ajaxModule}/{$ajaxActionNew}?ajax_embed_hash={$ajaxEmbedHash}",
          'success'  => "var container = ( jQuery('.ui-dialog').length && jQuery('.ui-dialog').css('display') != 'none' ) ? '.ui-dialog #ajax_popup_box #{$subFormId}.form_list' : '#{$subFormId}.form_list'; jQuery(container).append(data);",
          'position' => 'bottom',
          'method'   => 'get',
          'loading'  => "jQuery('#{$subFormId}_loading').toggle()",
          'complete'  => "jQuery('#{$subFormId}_loading').toggle();",
        ),
        array(
          'class'    => 'amButton hasAmButtonTheme_GreenOK'
        )) .
        tag('img', array(
          'id' => "{$subFormId}_loading",
          'src' => public_path('images/loading02.gif'),
          'style' => 'display: none',
        ))
      );

      if (isset($embedOptions['uniqueRelation']))
      {
        $subForm->mergePostValidator(new sfValidatorCallback(
          array(
            'callback' => array($this, 'checkUniqueRelation'),
            'arguments' => array(
              'relationName' => $relationName,
              'uniqueRelationName' => $embedOptions['uniqueRelation']
            )
          ),
          array('invalid' => 'Duplicate (non-unique) records detected!')
        ));
      }

      if (isset($embedOptions['uniqueColumn']))
      {
        $subForm->mergePostValidator(new sfValidatorCallback(
          array(
            'callback' => array($this, 'checkUniqueColumn'),
            'arguments' => array(
              'relationName' => $relationName,
              'uniqueColumnName' => $embedOptions['uniqueColumn']
            )
          ),
          array('invalid' => 'Duplicate (non-unique columns) records detected!')
        ));
      }

      $index = 1;
      foreach ($this->object[$relationName] as $childObject)
      {
        $tree = $options['embed_tree'];
        $set = array_pop($tree);
        $set[4] = $index;
        $tree[] = $set;
        $options['embed_tree'] = $tree;

        $form = new $formClass($childObject, $options);
        unset($form[$relation->getForeign()]);

        $ajaxEmbedList = array();
        $ajaxEmbedIdList = array();
        foreach ($options['embed_tree'] as $key => $values) {
          $ajaxEmbedList[] = implode('-', $values);
          $ajaxEmbedIdList[] = $values[1];
          if (isset($values[4]))
            $ajaxEmbedIdList[] = $values[4];
        }
        $ajaxEmbedHash = implode('--', $ajaxEmbedList);
        $ajaxEmbedId = implode('_', $ajaxEmbedIdList);

        $formId = $this->widgetSchema->generateId("{$ajaxEmbedId}_MANY[]");
        if ($formatter = $form->widgetSchema->getFormFormatter())
          $formatter->setDecoratorId($formId);

        $subForm->embedForm("{$index}", $form);
        $subForm->widgetSchema->setLabel("{$index}",
          jq_button_to_remote('Remove', array(
            'url'     => "{$ajaxModule}/{$ajaxActionDelete}?ajax_id={$childObject->id}",
            'success'   => "jQuery('.form_list > .form_list_item:has(#{$formId}):last').remove()",
            'confirm' => 'Are you sure?',
            'loading'  => "jQuery('#{$formId}_loading').toggle()",
            'complete'  => "jQuery('#{$formId}_loading').toggle();",
          ),
          array(
            'class'    => 'amButton hasAmButtonTheme_GreenOK'
          )).
          tag('img', array(
            'id' => "{$formId}_loading",
            'src' => public_path('images/loading02.gif'),
            'style' => 'display: none',
          ))
        );

        $index++;
      }

      $this->embedForm("{$relationName}_MANY", $subForm);
      $this->widgetSchema->setLabel("{$relationName}_MANY", $label);
      $this->validatorSchema["{$relationName}_MANY"]->setOption('required', false);
    }
  }

  public function embedRelationTree($newIndex, $embedHash, $embedTree, $options = array())
  {
    if (empty($embedTree))
      return array();

    $embedSet = $embedTree[0];
    unset($embedTree[0]);
    $embedTree = array_values($embedTree);

    $parentFormClass = $embedSet[0];
    $relationName = $embedSet[1];
    $foreignRelationName = $embedSet[2];
    $formClass = $embedSet[3];
    $index = isset($embedSet[4]) ? $embedSet[4] : $newIndex;

    $table = $this->object->getTable();
    if (!$table->hasRelation($relationName))
      throw new RuntimeException(sprintf('Relation "%s" does not exist on table "%s"', $relationName, $table->getTableName()));

    $relation = $table->getRelation($relationName);
    //$this->object->refreshRelated($relationName); // Dunno why I originally had this, but I cannot keep it

    $objectClass = $relation->getClass();

    if ($parentFormClass != get_class($this))
      throw new RuntimeException(sprintf('Invalid parent form "%s" for embedded tree', $parentFormClass));

    $formClass = is_null($formClass) ? $relation->getClass() . 'Form' : $formClass;
    if (!class_exists($formClass))
      throw new RuntimeException(sprintf('Form of class "%s" does not exist', $formClass));

    $options = sfForm::deepArrayUnion($this->options, $options);
    $options['embed_tree'][] = array($parentFormClass, $relationName, $foreignRelationName, $formClass);

    if ($relation->isOneToOne())
    {
      $form = $this->embeddedForms["{$relationName}_ONE"];

      $ajaxEmbedList = array();
      $ajaxEmbedIdList = array();
      foreach ($options['embed_tree'] as $key => $values) {
        $ajaxEmbedList[] = implode('-', $values);
        $ajaxEmbedIdList[] = $values[1];
        if (isset($values[4]))
          $ajaxEmbedIdList[] = $values[4];
      }
      $ajaxEmbedHash = implode('--', $ajaxEmbedList);
      $ajaxEmbedId = implode('_', $ajaxEmbedIdList);

      $formId = $this->widgetSchema->generateId("{$ajaxEmbedId}_ONE[]");
      if ($formatter = $form->widgetSchema->getFormFormatter())
        $formatter->setDecoratorId($formId);

      $returnIndex = $form->embedRelationTree($newIndex, $embedHash, $embedTree, $options);
      $returnIndexList = array_merge(array("{$relationName}_ONE"), $returnIndex);

      $label = $this->widgetSchema->getLabel("{$relationName}_ONE");
      $this->embedForm("{$relationName}_ONE", $this->embeddedForms["{$relationName}_ONE"]);
      $this->widgetSchema->setLabel("{$relationName}_ONE", $label);
    }
    else
    {
      $subForm = $this->embeddedForms["{$relationName}_MANY"];

      if (!isset($subForm["{$index}"]) && isset($options['reIndexNew']) && $options['reIndexNew'])
      {
        if (!isset($options['reIndexNewMap']) || !isset($options['reIndexNewMap']["{$relationName}_MANY_{$index}"]))
        {
          $newIndex = 1;
          while (isset($subForm["{$newIndex}"]))
            $newIndex++;
          $options['reIndexNewMap']["{$relationName}_MANY_{$index}"] = $newIndex;
          $this->options['reIndexNewMap'] = $options['reIndexNewMap'];
        }
        $index = $options['reIndexNewMap']["{$relationName}_MANY_{$index}"];
      }

      if (isset($subForm["{$index}"]))
      {
        $embeddedSubForms = $subForm->getEmbeddedForms();
        $form = $embeddedSubForms["{$index}"];
      }
      else
      {
        $childObject = new $objectClass();
        $childObject[$foreignRelationName] = $this->object;

        $tree = $options['embed_tree'];
        $set = array_pop($tree);
        $set[] = $index;
        $tree[] = $set;
        $options['embed_tree'] = $tree;

        $form = new $formClass($childObject, $options);
        unset($form[$relation->getForeign()]);

        $form->setWidget('ajax_index', new sfWidgetFormInputHidden(array(), array('value' => $index)));
        $form->setWidget('ajax_hash', new sfWidgetFormInputHidden(array(), array('value' => $embedHash)));
      }

      $ajaxEmbedList = array();
      $ajaxEmbedIdList = array();
      foreach ($options['embed_tree'] as $key => $values) {
        $ajaxEmbedList[] = implode('-', $values);
        $ajaxEmbedIdList[] = $values[1];
        if (isset($values[4]))
          $ajaxEmbedIdList[] = $values[4];
      }
      $ajaxEmbedHash = implode('--', $ajaxEmbedList);
      $ajaxEmbedId = implode('_', $ajaxEmbedIdList);

      $formId = $this->widgetSchema->generateId("{$ajaxEmbedId}_MANY[]");
      if ($formatter = $form->widgetSchema->getFormFormatter())
        $formatter->setDecoratorId($formId);

      $returnIndex = $form->embedRelationTree($newIndex, $embedHash, $embedTree, $options);
      $returnIndexList = array_merge(array("{$relationName}_MANY", "{$index}"), $returnIndex);

      $subForm->embedForm("{$index}", $form);
      $subForm->widgetSchema->setLabel("{$index}", jq_button_to_function('Remove',
        "if (confirm('Are you sure?')) jQuery('.form_list > .form_list_item:has(#{$formId}):last').remove()",
        array(
          'class' => 'amButton hasAmButtonTheme_RedCancel'
        )));

      $label = $this->widgetSchema->getLabel("{$relationName}_MANY");
      $this->embedForm("{$relationName}_MANY", $this->embeddedForms["{$relationName}_MANY"]);
      $this->widgetSchema->setLabel("{$relationName}_MANY", $label);
    }

    return $returnIndexList;
  }

  protected function embedAllRelationTrees($taintedValues, $parent = null)
  {
    if (!is_array($taintedValues))
      return $taintedValues;

    $newTaintedValues = array();
    $index = 1;
    foreach ($taintedValues as $key => $values)
    {
      if ($key === 'ajax_index')
      {
        $ajax_index = $taintedValues['ajax_index'];
        $ajax_embedHash = $taintedValues['ajax_hash'];

        $ajax_embedList = explode('--', $ajax_embedHash);
        $ajax_embedTree = array();
        foreach ($ajax_embedList as $key => $values)
          $ajax_embedTree[] = explode('-', $values);

        $this->embedRelationTree($parent, $ajax_embedHash, $ajax_embedTree, array(
          'reIndexNew' => true,
        ));
      }
      else if ($key === 'ajax_hash')
      {
      }
      else
      {
        $newKey = $key;
        if (strpos($parent, '_MANY') !== false)
          $newKey = $index++;

        $newTaintedValues[$newKey] = $this->embedAllRelationTrees($values, $key);
      }
    }

    return $newTaintedValues;
  }

  // Bind unbound ajax embedded forms
  public function bind(array $taintedValues = null, array $taintedFiles = null)
  {
    $newTaintedValues = $this->embedAllRelationTrees($taintedValues);
    $newTaintedFiles = $this->embedAllRelationTrees($taintedFiles);

    return parent::bind($newTaintedValues, $newTaintedFiles);
  }

  /*
  public function saveEmbeddedForms($con = null, $forms = null, $taintedValues = null, $taintedFiles = null)
  {
    if (is_null($con))
      $con = $this->getConnection();
    if (is_null($forms))
      $forms = $this->embeddedForms;
    if (is_null($taintedValues))
      $taintedValues = $this->taintedValues;
    if (is_null($taintedFiles))
      $taintedFiles = $this->taintedFiles;

    foreach ($forms as $key => $form) {
      if (isset($taintedValues[$key])) {
        if ($form instanceof sfFormDoctrine)
          $form->bindAndSave($taintedValues[$key], $taintedFiles);
        else
          $this->saveEmbeddedForms($con, $form->getEmbeddedForms(), $taintedValues[$key], $taintedFiles);
      }
    }
  }
  */

  public function checkUniqueColumn($oValidator, $values)
  {
    $arguments = $oValidator->getOption('arguments');

    $relationName = $arguments['relationName'];
    $uniqueColumnName = $arguments['uniqueColumnName'];

    $table = $this->object->getTable()->getRelation($relationName)->getTable();
    if(!$table->hasColumn($uniqueColumnName))
      throw new RuntimeException(sprintf('Column "%s" does not exist on table "%s"', $uniqueColumnName, $table->getTableName()));

    $ids = array();
    foreach ($values as $val)
      if (isset($val[$uniqueColumnName]))
        $ids[] = $val[$uniqueColumnName];
    $unique_ids = array_unique($ids);

    if (count($ids) !== count($unique_ids))
      throw new sfValidatorError($oValidator, 'invalid');

    return $values;
  }

  public function checkUniqueRelation($oValidator, $values)
  {
    $arguments = $oValidator->getOption('arguments');

    $relationName = $arguments['relationName'];
    $uniqueRelationName = $arguments['uniqueRelationName'];
    if(!is_array($uniqueRelationName)) $uniqueRelationName = array($uniqueRelationName);

    $table = $this->object->getTable()->getRelation($relationName)->getTable();
    foreach($uniqueRelationName as $anUniqueRelationName)
      if (!$table->hasRelation($anUniqueRelationName))
        throw new RuntimeException(sprintf('Relation "%s" does not exist on table "%s"', $anUniqueRelationName, $table->getTableName()));

    $submittedIds = array();
    foreach($uniqueRelationName as $anUniqueRelationName)
    {
      $uniqueColumnName = $table->getRelation($anUniqueRelationName)->getLocalColumnName();
      $itemCount = 0;
      foreach ($values as $val) {
        if (isset($val[$uniqueColumnName])) {
          if (!isset($submittedIds["item: $itemCount"])) $submittedIds["item: $itemCount"] = '';
            $submittedIds["item: $itemCount"] .= '###' . $val[$uniqueColumnName];
        }
        ++$itemCount;
      }
    }

    $uniqueIds = array_unique($submittedIds);

    if (count($submittedIds) !== count($uniqueIds))
      throw new sfValidatorError($oValidator, 'invalid');

    return $values;
  }
}
