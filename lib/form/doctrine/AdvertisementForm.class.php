<?php

/**
 * Advertisement form.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertisementForm extends BaseAdvertisementForm
{
  public function configure()
  {
    $this->widgetSchema['is_published']->setLabel('Published');
    $this->widgetSchema['location_id']  = new sfWidgetFormDoctrineChoiceNestedSet(array('model' => $this->getRelatedModelName('Location'), 'add_empty' => false));
    $this->widgetSchema['priority'] = new sfWidgetFormSelect(array('choices' => range(1,10)));
  }
}
