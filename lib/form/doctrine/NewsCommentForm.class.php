<?php

/**
 * NewsComment form.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsCommentForm extends BaseNewsCommentForm
{
  public function configure()
  {
      $this->useFields(array(
        'news_id',
        'title',
        'body',
        'is_published'
      ));

      $this->hideFields(array(
        'news_id'
      ));

    $this->widgetSchema['title'] = new sfWidgetFormAmKeyboardSwitcherInputText();
    $this->widgetSchema['body'] = new sfWidgetFormAmKeyboardSwitcherTextarea();
    $this->filterWidgetsByPermissions();
  }

  public function filterWidgetsByPermissions()
  {
      $user = $this->getOption('user', false);
      
      if(!$user || ($user && !$user->hasGroup('admin')) )
      {
        unset($this['is_published']);
      }
  }
}
