<?php

/**
 * sfGuardUserAdminForm for admin generators
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUserAdminForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardUserAdminForm extends BasesfGuardUserAdminForm
{
  public function setup()
  {
      parent::setup();
      
      unset($this['permissions_list']);
      
      $this->widgetSchema['state'] = new sfWidgetFormSelectUSState(array('add_empty' => true));
      $this->widgetSchema['country'] = new sfWidgetFormI18nChoiceCountry(array('culture' => 'en', 'add_empty' => true));
      $this->widgetSchema['groups_list'] = new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardGroup'));

      $years = range( date('Y') - 100, date('Y') - 12);
      $this->widgetSchema['date_of_birth']->setOption('years', array_combine($years, $years));

      $this->filterWidgetsByPermissions();
  }

  public function filterWidgetsByPermissions()
  {
      $user = $this->getOption('user', false);
      
      if(!$user || ($user && !$user->hasCredential('admin')) )
      {
        unset($this['groups_list']);
      }
  }
}
