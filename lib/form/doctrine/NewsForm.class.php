<?php

/**
 * News form.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsForm extends BaseNewsForm
{
  public function configure()
  {
      $this->offsetsUnset(array(
          'parent_id',
          'user_id',
      ));

     $this->widgetSchema['is_published']->setLabel('Published');
     $this->widgetSchema['title'] = new sfWidgetFormAmKeyboardSwitcherInputText();
     $this->widgetSchema['body'] = new sfWidgetFormAmKeyboardSwitcherTextarea();
     $this->widgetSchema['location_id']  = new sfWidgetFormDoctrineChoiceNestedSet(array('model' => $this->getRelatedModelName('Location'), 'add_empty' => false));

     $this->widgetSchema['priority'] = new sfWidgetFormSelect(array('choices' => range(1,10)));

     /* finish me... add multiple image options */
     $this->embedRelationForm('Images', 'News');

     $this->filterWidgetsByPermissions();
     $this->setDefaultValues();
  }

  public function filterWidgetsByPermissions()
  {
      $user = $this->getOption('user', false);
      
      if(!$user || ($user && !$user->hasGroup('admin')) )
      {
        unset($this['priority']);
        unset($this['is_published']);
      }
  }
  
  public function setDefaultValues()
  {
    $location_identifier = $this->getOption('location_identifier', false);
    $location = Doctrine::getTable('NewsLocation')->getLocationByIdentifier($location_identifier);
    
    if($location)
    {
      $this->setDefault('location_id', $location->id);
    }
  }
}
