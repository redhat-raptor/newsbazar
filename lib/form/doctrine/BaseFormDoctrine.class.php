<?php

/**
 * Project form base class.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormDoctrine extends UBNBaseFormDoctrine
{
  public function setup()
  {
    $this->unsetAutofields();
    
    $amFormFormatter = new amWidgetFormSchemaFormatterList($this->widgetSchema);
    $amFormFormatter->setValidatorSchema($this->validatorSchema);
    $this->widgetSchema->addFormFormatter('amList', $amFormFormatter);

    //donno why the following is not working on the register page
    //$this->widgetSchema->setDefaultFormFormatterName('amList');
    parent::setup();
  }
  
  protected function unsetAutofields()
  {
    $autofieldsList = array(
      'created_at',
      'updated_at',
    );
    
    $this->offsetsUnset($autofieldsList);
  }

  protected function filterWidgetsByPermissions()
  {
  }
}
