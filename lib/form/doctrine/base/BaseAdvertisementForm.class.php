<?php

/**
 * Advertisement form base class.
 *
 * @method Advertisement getObject() Returns the current form's model object
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAdvertisementForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'caption'      => new sfWidgetFormInputText(),
      'filename'     => new sfWidgetFormInputText(),
      'location_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Location'), 'add_empty' => false)),
      'priority'     => new sfWidgetFormInputText(),
      'is_published' => new sfWidgetFormInputCheckbox(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'caption'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'filename'     => new sfValidatorString(array('max_length' => 255)),
      'location_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Location'))),
      'priority'     => new sfValidatorInteger(array('required' => false)),
      'is_published' => new sfValidatorBoolean(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('advertisement[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Advertisement';
  }

}
