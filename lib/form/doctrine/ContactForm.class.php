<?php

/**
 * Contact form.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ContactForm extends BaseContactForm
{
  public function configure()
  {
     $this->validatorSchema['email'] = new sfValidatorEmail();
     $this->filterWidgetsByPermissions();
  }

  public function filterWidgetsByPermissions()
  {
      $user = $this->getOption('user', false);

      if(!$user || ($user && !$user->hasGroup('admin')) )
      {
        unset($this['is_read']);
      }
  }
}
