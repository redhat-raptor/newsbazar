<?php

/**
 * Base project form.
 * 
 * @package    ubn-sym
 * @subpackage form
 * @author     . 
 * @version    SVN: $Id: BaseForm.class.php 20147 2009-07-13 11:46:57Z FabianLange $
 */
class BaseForm extends sfFormSymfony
{
    public function offsetsUnset($offsets) {
        foreach($offsets as $anOffset)
            if($this->offsetExists($anOffset))
                $this->offsetUnset($anOffset);
    }

    public function hideFields($offsets) {
        foreach($offsets as $anOffset)
            if($this->offsetExists($anOffset))
                $this->widgetSchema[$anOffset] = new sfWidgetFormInputHidden();
    }
}