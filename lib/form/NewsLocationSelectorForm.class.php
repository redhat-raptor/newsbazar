<?php

/**
 * News form.
 *
 * @package    ubn-sym
 * @subpackage form
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsLocationSelectorForm extends BaseNewsForm
{
  public function configure()
  {   
      $this->useFields(array(
          'location_id',
      ));

      $this->setWidget('location_id', new sfWidgetFormDoctrineChoiceNestedSet(array(
        'model'     => 'NewsLocation',
        'key_method' => 'getPathURI'
      )));
  }
}