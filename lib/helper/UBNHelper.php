<?php

function url_for_location_uri($locationURI, $absolute = false)
{
    $locationRoute = '@news_location_path';
    $locationLink = url_for($locationRoute, $absolute) . '/' . $locationURI;
    return $locationLink;
}

function link_to_location($title, $locationURI, $absolute = false)
{
    $locationURL = url_for_location_uri($locationURI, $absolute);
    return link_to($title, $locationURL);
}

function url_list_for_path_list($locationPathList, $absolute = false)
{
    if(get_class($locationPathList) == 'sfOutputEscaperArrayDecorator')
        $list = convertSfOutputEscaperArrayDecoratorToArray($locationPathList);

    $URLList = array();
    $MAX = count($list)-1;

    for($i=0; $i<=$MAX; $i++)
        $URLList[$i] = $i == 0 ? url_for_location_uri($list[$i], $absolute) : $URLList[$i-1] . '/' . $list[$i]; //we have a location URI here

    if(get_class($locationPathList) == 'sfOutputEscaperArrayDecorator')
        return new sfOutputEscaperArrayDecorator(sfConfig::get('sf_escaping_method', 'ESC_SPECIALCHARS'), $URLList);

    return $URLList;
}

function convertSfOutputEscaperArrayDecoratorToArray(sfOutputEscaperArrayDecorator $escaperArray)
{
    $list = array();
    $escaperArray->rewind(); //sets the pointer to the first element
    
    while($escaperArray->valid())
    {
        $list[$escaperArray->key()] = $escaperArray->current();
        $escaperArray->next();
    }

    return $list;
}

function ubn_image_url($image, $maxWidth = null, $maxHeight = null, $bgcolor = null)
{
    $width = $maxWidth ? $maxWidth : sfConfig::get('app_ubn_max_image_width');
    $height = $maxHeight ? $maxHeight : sfConfig::get('app_ubn_max_image_height');

    $imageName = base64_encode($image);

    if($bgcolor)
        return url_for("@ubn_image_bgcolor?name=$imageName&width=$width&height=$height&bgcolor=$bgcolor");
    else
        return url_for("@ubn_image?name=$imageName&width=$width&height=$height");
}

function ubn_image_tag($image, $options = array(), $maxWidth = null, $maxHeight = null, $bgcolor = null)
{
    $width = $maxWidth ? $maxWidth : sfConfig::get('app_ubn_max_thumbnail_width');
    $height = $maxHeight ? $maxHeight : sfConfig::get('app_ubn_max_thumbnail_height');
    
    //return image_tag(ubn_image_url($image, $width, $height, $bgcolor), $options);
    $url = ubn_image_url($image, $width, $height, $bgcolor);
    return "<img src='$url' alt='image' />";
}

function ubn_image_link($image, $link, $options = array(), $maxWidth = null, $maxHeight = null, $bgcolor = null)
{
    $tag = ubn_image_tag($image, $options, $maxWidth, $maxHeight, $bgcolor);

    return link_to($tag, $link, $options);
}

function ubn_image_lightbox($image, $options = array(), $maxWidth = null, $maxHeight = null, $bgcolor = null)
{
    $tag = ubn_image_tag($image, $options, $maxWidth, $maxHeight, $bgcolor);

    if (!isset($options['class']))
        $options['class'] = 'lightbox';
    else
        $options['class'] .= ' lightbox';

    return link_to($tag, ubn_image_url($image), $options);
}