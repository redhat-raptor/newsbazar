<?php

class UBNHelper
{
    public static function pre_r($param)
    {
        if(is_object($param) && get_class($param) == 'sfOutputEscaperArrayDecorator')
            $param = convertSfOutputEscaperArrayDecoratorToArray($param);

        print '<pre>';
        print_r($param);
        print '</pre>';
    }
    
    public static function getUploadPath()
    {
        return sfConfig::get('sf_root_dir') . '/' . sfConfig::get('app_ubn_upload_path');
    }

    public static function getImageRelativeSize(sfImage $image, $maxWidth, $maxHeight)
    {
        $imageHeight = $maxHeight;
        $imageWidth = $maxWidth;

        if($image->getHeight() > $image->getWidth())
            $imageWidth = $image->getWidth() / $image->getHeight() * $maxHeight;

        else if($image->getWidth() > $image->getHeight())
            $imageHeight = $image->getHeight() / $image->getWidth() * $maxWidth;

        return array(
            'width' => $imageWidth,
            'height' => $imageHeight
        );
    }


}