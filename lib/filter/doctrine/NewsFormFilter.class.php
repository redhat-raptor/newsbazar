<?php

/**
 * News filter form.
 *
 * @package    ubn-sym
 * @subpackage filter
 * @author     .
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsFormFilter extends BaseNewsFormFilter
{
  public function configure()
  {
    $this->useFields(array(
      'title',
      'body',
      'location_id'
    ));
    
    //FIXME: needs special widget for filter with bengali
    //$this->widgetSchema['title'] = new sfWidgetFormAmKeyboardSwitcherInputText();
    //$this->widgetSchema['body'] = new sfWidgetFormAmKeyboardSwitcherInputText();
  }
}
